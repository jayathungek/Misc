import cv2
import random
import numpy as np
import math 
import time
import sys as Sys


def printProgress (iteration, total, prefix = '', suffix = '', decimals = 2, barLength = 100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
    """
    filledLength    = int(round(barLength * iteration / float(total)))
    percents        = round(100.00 * (iteration / float(total)), decimals)
    bar             = '#' * filledLength + '-' * (barLength - filledLength)
    Sys.stdout.write('%s [%s] %s%s %s\r' % (prefix, bar, percents, '%', suffix)),
    Sys.stdout.flush()
    if iteration == total:
        print("\n")



def colourFilter(img,b,g,r):	
	i =0
	for x in img:
	    j = 0
	    if i%2 == 0:
		    for y in x:
		        img[i][j] = [b,g,r]
		        j+=1
	    i+=1
	return img

def shuffle(img):
	i = 0
	shape = np.shape(img)
	temp1 = np.zeros(shape)
	temp2 = img
	n = len(img)
	while n > 0:
		index = random.randint(0,n)		
		temp1[i]=temp2[index]
		np.delete(temp2,index)		
		n-=1
		i+=1
	img = temp1
	return img

def shuffle2(img,lwb_x,lwb_y,upb_x,upb_y):
	lwb_y = int((float(lwb_y)/100)*len(img))
	upb_y = int((float(upb_y)/100)*len(img))-1
	lwb_x = int((float(lwb_x)/100)*len(img[0]))
	upb_x = int((float(upb_x)/100)*len(img[0]))-1
	
	i = lwb_y
	shape = np.shape(img)
	temp1 = np.zeros(shape)
	temp2 = img
	n = upb_y
	while n > lwb_y:
		index = random.randint(lwb_y,upb_y)		
		temp1[i]=temp2[index]
		np.delete(temp2,index)		
		n-=1
		i+=1
	
	i = 0
	while i < len(img)-1:
		if i==lwb_y:
			i=upb_y
		temp1[i]=img[i]
		i = i+1

	img = temp1
	return img

def getValue(pixel,bgr):
	if bgr == "b":
		return pixel[0]
	elif bgr == "g":
		return pixel[1]
	elif bgr == "r":
		return pixel[2]
	elif bgr == "ind":
		return pixel[3]

def getBrightness(pixel):
	return (pixel[0]+pixel[1]+pixel[2])/3

def sortImage(img,lwb_x,lwb_y,upb_x,upb_y,by):
	print len(img)
	size = len(img)
	shape = np.shape(img)
	returnImg = np.zeros(shape)
	lwb_x = (lwb_x/100)*len(img[0])
	upb_x = (upb_x/100)*len(img[0])-1
	lwb_y = (lwb_y/100)*len(img)
	upb_y = (upb_y/100)*len(img)-1
	i=0
	printProgress(i, size, prefix = 'Sorting Image: ', suffix = 'Complete', barLength = 50)
	for row in img:
		if i > lwb_y and i < upb_y:
			returnImg[i]=partialSort(row,lwb_x,upb_x,by)		
		else:
			returnImg[i]=row
		i+=1
		printProgress(i, size, prefix = 'Sorting Image: ', suffix = 'Complete', barLength = 50)
	img = returnImg
	return img

def sortImageSlow(img):
	size = len(img)
	step = len(img)/100
	shape = np.shape(img)
	returnImg = np.zeros(shape)
	i=0
	for row in img:
		returnImg[i]=sortRow(row)		
		i+=1
		if i%step == 0:
			print str(i*100/size)+"%"
	img = returnImg
	return img

def sortRow(row):
	shape = np.shape(row)
	returnRow = np.zeros(shape)
	temp = row
	i=0	
	while i < len(row):
		maxSoFar = 0
		maxIndex = 0
		j = 0
		for pixel in temp:
			value = getValue(pixel,"b")
			if value > maxSoFar:
				maxSoFar = value
				maxIndex = j
				returnRow[i] = pixel				
			j+=1
		temp[maxIndex] = 0
		i+=1
	return returnRow

def merge(row1,row2,by):
	shape = (np.shape(row1)[0]+np.shape(row2)[0],np.shape(row2)[1])
	new=np.zeros(shape)
	j=0
	i1=0
	i2=0
	leftoverList = np.array([])
	leftoverIndex = 0
	while j<len(row1)+len(row2):
		try:
			if getValue(row1[i1],by) >= getValue(row2[i2],by):
				new[j] = row1[i1]
				i1 += 1
				if i1 == len(row1):
					leftoverList = row2
					leftoverIndex = i2

			elif getValue(row2[i2],by) > getValue(row1[i1],by):
				new[j] = row2[i2]
				i2 += 1
				if i2 == len(row2):
					leftoverList = row1
					leftoverIndex = i1
		except IndexError:
			new[j] = leftoverList[leftoverIndex]
			leftoverIndex+=1
			
		finally:	
		    j+=1
	return new

def sort(row,by):
	if len(row) > 1:
		list1 = row[0:len(row)/2]
		list2 = row[len(row)/2:]
		return merge(sort(list1,by),sort(list2,by),by)
	else:
		return row

def partialSort(row,lwb,upb,by):
	shape = np.shape(row)
	new=np.zeros(shape)
	sublist = row[lwb:upb]
	sublist = sort(sublist,by)
	i=0
	while i < len(row):
		if i<lwb:
			new[i] = row[i]
		elif i == lwb:
			j = lwb
			for pixel in sublist:
				new[j] = pixel
				j+=1
			i = upb
		if i>=upb:
			new[i] = row[i]
		i+=1
	return new

def shiftRow(row,degree):
	shape = np.shape(row)
	new=np.zeros(shape)

	shift_factor = int(degree*len(row))
	sublist = row[0:shift_factor]
	sublist1 = row[shift_factor:]

	i = 0
	while i<len(sublist1):
		new[i]=sublist1[i]
		i+=1
	j=0
	while j<len(sublist):
		new[i]=sublist[j]
		j+=1
		i+=1

	return new

def shiftImage(img,amount_x):
	q1 = int(0.25*len(img))
	q2 = int(0.5*len(img))
	q3 = int(0.75*len(img))
	q4 = len(img)

	shape = np.shape(img)
	returnImg = np.zeros(shape)
	i=0
	for row in img:
		if (i>=0 and i<q1) or (i>=q2 and i<q3):
			returnImg[i] = shiftRow(row,amount_x)
		elif((i>=q1 and i<q2) or (i>=q3 and i<q4)):
			returnImg[i] = shiftRow(row,1-amount_x)
		i+=1
		print i
	img = returnImg
	return img

def imgToAux(img):
	shape = np.shape(img)
	shape = (shape[0],shape[1],4)
	aux = np.zeros(shape)


	i = 0
	for row in img:
		j = 0
		for pixel in row:
			aux[i][j][0] = pixel[0]
			aux[i][j][1] = pixel[1]
			aux[i][j][2] = pixel[2]
			aux[i][j][3] = j
			j += 1
		i += 1

	return aux

def auxToImg(aux):
	shape = np.shape(aux)
	shape = (shape[0],shape[1],3)
	img = np.zeros(shape)


	i = 0
	for row in aux:
		j = 0
		for pixel in row:
			img[i][j][0] = pixel[0]
			img[i][j][1] = pixel[1]
			img[i][j][2] = pixel[2]
			j += 1
		i += 1

	return img

def swipe(img,pos):
	pos = int((pos/100.00)*(len(img)-1))
	
	start = img[pos]
	i = 0
	for row in img:
		if i > pos:
			img[i] = start
		i+=1
	return img



img = cv2.imread("mona.jpg")
# img = imgToAux(img)
#colourFilter(img,200,55,200)
# new = shuffle2(img,12,40,43,50)
# new = sortImage(img,0,0,100,100,"r")
# new = sortImage(img,0,0,100,100,"ind")
# img = auxToImg(img)
new = shiftImage(img,0.00525)
# new = swipe(img,23)


num = str(random.randint(1,512))
cv2.imwrite("output%s.jpg"%(num),new)
# test = img[0]
# print test
# print partialSort(test,100,550,"b")
