import math

def merge(l1,l2):
	new=[]	
	i1=0
	i2=0
	leftoverList = []
	leftoverIndex = 0
	while len(new)<len(l1)+len(l2):
		try:
			if l1[i1] <= l2[i2]:
				new.append(l1[i1])
				i1 += 1
				if i1 == len(l1):
					leftoverList = l2
					leftoverIndex = i2

			elif l2[i2] <= l1[i1]:
				new.append(l2[i2])
				i2 += 1
				if i2 == len(l2):
					leftoverList = l1
					leftoverIndex = i1
		except IndexError:
			new.append(leftoverList[leftoverIndex])
			leftoverIndex+=1

		
	return new




def sort(row):
	if len(row) > 1:
		list1 = row[0:len(row)/2]
		# print list1
		list2 = row[len(row)/2:]
		# print list2
		return merge(sort(list1),sort(list2))
	else:
		return row

def partialSort(row,lwb,upb):
	new = []
	sublist = row[lwb:upb]
	sublist = sort(sublist)
	print sublist
	i=0
	while i < len(row):
		if i<lwb:
			new.append(row[i])
		elif i == lwb:
			new+=sublist
			i=upb
		if i>=upb:
			new.append(row[i])
		i+=1
	return new


print partialSort([1,120,112,12,2,3],0,5)
