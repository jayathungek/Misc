from __future__ import print_function
import datetime

def addSecs(tm, secs):
    fulldate = datetime.datetime(10, 1, 1, tm.hour, tm.minute, tm.second)
    fulldate = fulldate + datetime.timedelta(seconds=secs)
    return str(fulldate.time())


def dateFixer(tm):
	fixed_tm_with_ms = []
	fixed_tm = ""
	ms = ""
	found_ms = False
	for char in tm:
	    if char != ',' and not found_ms:
	    	fixed_tm += char
	    else:
	    	found_ms = True
	    	ms += char
	fixed_tm_with_ms.append(fixed_tm)
	fixed_tm_with_ms.append(ms[:-1]) 
	return fixed_tm_with_ms


def stringToDate(st):
	values = st.split(':')
	i = 0
	for value in values:
		value = int(value)
		values[i] = value
		i+=1

	fulldate = datetime.datetime(10,1,1,values[0],values[1],values[2])
	return fulldate.time()
	

def lineFixer(line):
	fixed_line = ""
	times = line.split(' ')
	for item in times:
		try:
			fixed = addSecs(stringToDate(dateFixer(item)[0]),25)
			fixed_line += fixed
			fixed_line += dateFixer(item)[1]
		except:
			fixed_line += " --> "
	return fixed_line
  



with open("copy.srt", "r") as ins:
	with open('fixed.srt', 'a') as out:
		for line in ins:
			if line[0] == '0':
				fixed_line = lineFixer(line)
				print(fixed_line, file=out)
			else:
				line = line[:-1]
				print(line, file=out)
