1
00:00:42,080 --> 00:00:46,080
An Deutschland konnte ich mich
gar nicht mehr richtig erinnern.

2
00:00:46,120 --> 00:00:50,120
Ich wusste noch,
dass es da Schnee gibt und Jahreszeiten.

3
00:00:50,160 --> 00:00:54,240
Und das unsere Familie da war.
Alle, nicht nur Mama und Papa. Eben alle.

4
00:00:54,280 --> 00:00:57,760
Und dass mir das gefallen hat.

5
00:00:59,320 --> 00:01:03,320
Aber ich wei� auch noch, dass ich
in Deutschland immer Angst hatte.

6
00:01:03,360 --> 00:01:09,680
Vor anderen Kindern und vor den Menschen
auf der Stra�e. Und sogar vor Hunden.

7
00:01:14,160 --> 00:01:16,400
Na komm. Komm, ich helf dir.

8
00:01:29,520 --> 00:01:34,520
Deutschland ist ein dunkler Ort f�r mich.
Nicht so hell und hei� wie Kenia.

9
00:01:34,560 --> 00:01:38,520
Ein Ort mit gro�en Geb�uden
und mit d�steren Zimmern.

10
00:01:38,560 --> 00:01:43,560
Deutschland ist unsere Heimat, sagt Papa.
Er vermisst wohl vor allem seinen Vater.

11
00:01:43,600 --> 00:01:48,560
Meinen Opa. Er hatte eine Nussallergie
und durfte keine Nusspl�tzchen essen.

12
00:01:48,600 --> 00:01:54,440
Er mochte Heinrich Heines Gedichte gern.
Vor allem die �ber Deutschland.

13
00:01:57,000 --> 00:02:00,320
Ich hatte auch zwei Tanten dort,
die ich furchtbar gern mochte.

14
00:02:00,360 --> 00:02:04,360
Tante K�the war Mamas Schwester.
Papas Schwester hie� Liesel.

15
00:02:04,400 --> 00:02:08,360
Manchmal halfen sie in Gro�vaters
Hotel "Adler" aus.

16
00:02:08,400 --> 00:02:12,960
Aber das war in den guten Zeiten,
bevor die Nazis kamen.

17
00:02:19,600 --> 00:02:22,600
Soll ich Ihnen helfen?

18
00:02:22,640 --> 00:02:25,640
Nein, danke.

19
00:02:37,920 --> 00:02:40,920
Wir m�ssen jetzt fahren.

20
00:02:43,680 --> 00:02:47,680
Die Nazis verboten Papa,
als Rechtsanwalt zu arbeiten

21
00:02:47,720 --> 00:02:52,240
und nahmen Gro�vater das Hotel weg.
Dann war die ganze Familie arbeitslos.

22
00:02:52,280 --> 00:02:57,320
Fragte ich meine Eltern, warum das so ist,
bekam ich immer die gleiche Antwort:

23
00:02:57,360 --> 00:03:00,400
Weil wir Juden sind, Regina. Deshalb.

24
00:03:48,320 --> 00:03:51,320
Bwana! Bwana! Bwana!

25
00:03:52,440 --> 00:03:55,440
Bwana! Bwana! Bwana!

26
00:03:57,560 --> 00:04:01,560
- Was ist denn los?
- Eine eilige Nachricht.

27
00:04:26,920 --> 00:04:28,920
Walter.

28
00:04:29,160 --> 00:04:32,760
Walter! Wach auf!
Das ist Chinin! Das ist gut.

29
00:04:35,440 --> 00:04:38,440
Komm, wach auf!

30
00:04:46,280 --> 00:04:50,000
Menschenskinder, Walter, wach auf!

31
00:05:03,560 --> 00:05:06,640
Walter, mach den Mund auf!

32
00:05:12,640 --> 00:05:17,040
Entschuldigung, ich muss mal eben
die T�r aufmachen. Augenblick.

33
00:05:17,080 --> 00:05:22,240
Tante Jettel, gibt es noch Apfelsaft?
Kleinen Augenblick. Ja?

34
00:05:23,480 --> 00:05:28,400
Anna! Sch�n dass ihr kommen konntet
bei dem Schnee. Rudolf, kommt rein.

35
00:05:28,440 --> 00:05:32,480
'N Abend, Frau Redlich.
Du sollst doch nicht mit den reden!

36
00:05:34,880 --> 00:05:41,280
Anna, hallo.
Hallo, Ina.

37
00:05:42,480 --> 00:05:46,800
Hubert, J�rgen! H�rt auf zu z�ndeln!
Ihr steckt ja noch das ganze Haus an.

38
00:05:46,840 --> 00:05:49,840
Ja, ja, Klara.

39
00:05:52,280 --> 00:05:56,360
Hier, bei� mal.
Daran wirst du dich gew�hnen m�ssen.

40
00:05:56,400 --> 00:06:01,240
Die Neger essen alles so verbrannt.
Oder ganz roh. Und blutig!

41
00:06:01,280 --> 00:06:05,280
Aber Hunde essen die schon!
Das wei� ich aus meinem Erdkundebuch.

42
00:06:05,320 --> 00:06:09,920
Die essen ja auch Heuschrecken. Halt mal!
Was willst du denn in Afrika?

43
00:06:09,960 --> 00:06:15,400
Du traust dich doch nicht mal,
einen Dackel anzufassen.

44
00:06:15,440 --> 00:06:19,440
Ist das mit N�ssen?
Es gab nur das eine, Herr Redlich!

45
00:06:19,480 --> 00:06:23,440
Aber, Klara.
So viele N�sse sind ja nicht drin!

46
00:06:23,480 --> 00:06:27,440
Weil du davon niesen musst, gell?
Ja, bei N�ssen muss ich niesen.

47
00:06:27,480 --> 00:06:30,480
Jedes verflixte Mal.

48
00:06:32,160 --> 00:06:36,560
Das Gr�ne hab ich geliebt. Walter hatte
es mir mal zu Silvester gekauft.

49
00:06:36,600 --> 00:06:40,680
Aber irgendwie steht's mir nicht mehr.
Ich find's todschick.

50
00:06:40,720 --> 00:06:43,720
Dann nimm's.

51
00:06:45,240 --> 00:06:48,240
Bist 'n Schatz, Jettel. Danke.

52
00:06:48,760 --> 00:06:53,240
K�thchen. Keine Tr�nen heute, ja?
Mal ehrlich, kann ich die tragen?

53
00:06:53,280 --> 00:06:57,360
Steh halt zu deinem Hintern, Liesel.
M�nner m�gen's rund. Frau Redlich?

54
00:06:57,400 --> 00:07:01,360
Das ist was f�r Sie, Klara.

55
00:07:01,400 --> 00:07:04,400
Ein bisschen Glitzer tut Ihnen gut.

56
00:07:04,440 --> 00:07:07,880
Was ist?

57
00:07:08,640 --> 00:07:11,640
Der ist aus Afrika.

58
00:07:16,720 --> 00:07:19,880
Rongai, Kenia, 2. Dezember 1937.

59
00:07:20,280 --> 00:07:25,280
<i>Geliebte Jettel. Ich kann mir denken,
wie aufgeregt du �ber diesen Brief bist,</i>

60
00:07:25,320 --> 00:07:29,320
<i>aber ich bitte dich, jetzt stark zu sein.
Die j�dische Gemeinde in Nairobi</i>

61
00:07:29,360 --> 00:07:34,320
<i>�bernimmt auch die Geb�hren f�r
eure Einwanderung. Nun ist es soweit.</i>

62
00:07:34,360 --> 00:07:39,920
<i>Nach 6 Monaten kann ich Regina und dich
endlich hierher nachkommen lassen.</i>

63
00:07:39,960 --> 00:07:44,080
<i>Bitte z�gere keinen Tag l�nger!
Geh sofort zu Karl Silbermann.</i>

64
00:07:44,120 --> 00:07:49,080
<i>Er kann dir mit den Schiffskarten helfen.
Sag ihm, welches Schiff es ist und</i>

65
00:07:49,120 --> 00:07:54,120
<i>wie lange es unterwegs ist, ist egal.
Hauptsache, es nimmt euch beide mit.</i>

66
00:07:54,160 --> 00:07:59,160
<i>Wir brauchen dringend einen Eisschrank.
Wenn er nicht mehr in unsere Kisten passt,</i>

67
00:07:59,200 --> 00:08:03,240
<i>schmei� daf�r das Rosenthal-Geschirr raus.
Das ist hier v�llig unbedeutend.</i>

68
00:08:03,280 --> 00:08:06,960
<i>Besorge noch ein paar Petromaxlampen,
au�erdem Moskitonetze und feste Schuhe</i>

69
00:08:07,000 --> 00:08:11,360
<i>f�r dich und Regina. Versuch ja nicht,
Bargeld oder Schmuck mitzunehmen.</i>

70
00:08:11,400 --> 00:08:15,800
<i>Du kennst die Nazis. Sprich so wenig
wie m�glich �ber eure Pl�ne.</i>

71
00:08:15,840 --> 00:08:19,840
<i>Man kann niemandem mehr trauen,
auch nicht den Menschen,</i>

72
00:08:19,880 --> 00:08:24,880
<i>die eben noch unsere besten Freunde waren.
Mein Herz zerspringt bei dem Gedanken,</i>

73
00:08:24,920 --> 00:08:29,920
<i>euch beide bald in die Arme zu schlie�en.
Es wird schwer, wenn ich mir vorstelle,</i>

74
00:08:29,960 --> 00:08:33,560
<i>wie weh dieser Brief
deiner Mutter tun wird.</i>

75
00:08:35,680 --> 00:08:38,880
Walter? H�r zu! Du hast Malaria.

76
00:08:39,280 --> 00:08:43,680
Du musst dieses Chinin hier nehmen.
H�rst du, das ist ganz wichtig!

77
00:08:43,720 --> 00:08:47,720
Ich muss zur�ck zur Farm.
Owuor wird sich um dich k�mmern.

78
00:09:15,400 --> 00:09:19,640
Max, was ist? Wir m�ssen los!
Ich hasse Abschiede.

79
00:09:19,680 --> 00:09:22,880
Und Bahnh�fe kann ich auch nicht leiden.

80
00:09:25,680 --> 00:09:29,680
In ein, zwei Jahren
ist das hier doch alles vorbei.

81
00:09:37,760 --> 00:09:41,160
Ihr haltet zusammen, ihr zwei, ja?

82
00:09:42,680 --> 00:09:46,440
Versprich mir,
dass ihr das zusammen durchsteht.

83
00:09:51,320 --> 00:09:55,240
Einer liebt immer mehr.
Das macht's so schwierig.

84
00:09:56,960 --> 00:10:00,080
Und der, der mehr liebt ist verwundbar.

85
00:10:07,080 --> 00:10:10,240
Mein Sohn liebt dich. Sehr.

86
00:10:13,960 --> 00:10:16,280
Du wirst mir so fehlen, Max.

87
00:10:23,560 --> 00:10:25,560
Mama!

88
00:10:44,320 --> 00:10:47,880
Sag ihr Auf Wiedersehen. Bitte.

89
00:11:51,680 --> 00:11:56,640
Kennst du Schlesisches Himmelreich?
Schlesische Himme...

90
00:11:56,680 --> 00:12:01,960
Fleisch, Kartoffeln, Speck, D�rrobst,
alles zusammengemischt. Mmh!

91
00:12:07,760 --> 00:12:11,760
In meinem ersten Leben war ich Anwalt,
wei�t du?

92
00:12:14,440 --> 00:12:18,520
Kikombe. Kikombe!

93
00:12:18,560 --> 00:12:20,560
Kikombe.

94
00:12:20,680 --> 00:12:24,640
Kiyo la dirisha.
Kiyo la dirisha.

95
00:12:24,680 --> 00:12:27,680
Kiyo la di...

96
00:12:28,480 --> 00:12:31,480
Dirisha la Kiyo.

97
00:12:32,120 --> 00:12:35,360
Asante kapissa.

98
00:13:44,080 --> 00:13:47,080
Owuor? Das ist f�r dich.

99
00:13:50,960 --> 00:13:54,480
Ich war krank und du hast mich gepflegt.

100
00:13:58,320 --> 00:14:02,320
Ich hab diese Robe
in meiner Heimat getragen.

101
00:14:05,400 --> 00:14:08,680
Sondern mit meinem Kopf gearbeitet habe.

102
00:14:14,440 --> 00:14:17,600
Nein. F�r eine Robe muss man klug sein.

103
00:14:21,560 --> 00:14:24,080
Hm.

104
00:14:39,080 --> 00:14:43,640
6 Wochen fuhren wir mit einem
Passsagierdampfer einmal um ganz Afrika.

105
00:14:43,680 --> 00:14:47,800
Am 20. April 1938, dem Geburtstag
des F�hrers, kamen wir in Nairobi an.

106
00:14:47,840 --> 00:14:50,840
Regina, nimm das mal bitte.

107
00:14:54,200 --> 00:14:57,320
Mr. Morrison, der Besitzer der Rinderfarm,
auf der Papa arbeitete, holte uns ab.

108
00:15:21,600 --> 00:15:25,600
Ich musste an Schokolade denken.
Aber Mama erkl�rte mir auf dem Schiff,

109
00:15:25,640 --> 00:15:29,880
dass wir jetzt arm waren. Und f�r arme
Kinder gibt es keine Schokolade.

110
00:15:29,920 --> 00:15:33,360
Ich hatte ihr versprochen,
tapfer zu sein.

111
00:17:01,640 --> 00:17:05,240
Liebste!

112
00:17:42,840 --> 00:17:47,240
Regina, mein M�dchen! Komm zu mir!
Papa! Wir sind mit dem Schiff gefahren!

113
00:17:47,280 --> 00:17:52,120
Und waren in England und Marokko und
�berall am Hafen. Ich hab Delfine gesehen!

114
00:17:52,160 --> 00:17:56,160
Und Mama hat auch mal getanzt!

115
00:19:03,480 --> 00:19:05,480
Kinderlachen

116
00:19:25,760 --> 00:19:29,760
Es gibt zu wenig Wasser f�r die Rinder.
F�r welche Rinder?

117
00:19:29,800 --> 00:19:33,960
F�r alle Rinder.
Dein Vater hat die Papiere besorgt.

118
00:19:34,000 --> 00:19:39,000
Und die Fluchtsteuer. Wir hatten Gl�ck.
In Hamburg �ffneten sie nur eine Kiste.

119
00:19:39,040 --> 00:19:43,040
Da war nur W�sche drin. Max gab mir
den "Zauberberg" f�r dich mit.

120
00:19:43,080 --> 00:19:47,640
Die B�cher waren in der anderen Kiste.
Gott sei Dank.

121
00:19:47,680 --> 00:19:51,720
Ich wei�, es ist nicht das,
was du dir erwartet hast.

122
00:19:51,760 --> 00:19:54,760
Es ist wundersch�n.

123
00:19:55,920 --> 00:19:59,040
Aber hier k�nnen wir doch nicht leben.

124
00:20:11,280 --> 00:20:15,600
Die Sachen mit dem Muster nicht.
Das brauchen wir nicht alles auspacken.

125
00:20:15,640 --> 00:20:18,280
Wir bleiben ja nicht lange hier.

126
00:20:32,720 --> 00:20:36,040
Memsaab. Teller. Ein Teller.

127
00:20:36,680 --> 00:20:42,200
Wenn du mit mir reden willst,
musst du schon Deutsch lernen. Teller!

128
00:20:47,520 --> 00:20:51,360
Vorsichtig! Ja.

129
00:20:52,520 --> 00:20:54,520
Teller!

130
00:21:24,080 --> 00:21:27,080
Jettel? Mein Name ist S��kind.

131
00:21:27,720 --> 00:21:31,240
Auch Walter.
Aber alle nennen mich S��kind.

132
00:21:31,400 --> 00:21:33,960
Ach so.

133
00:21:34,000 --> 00:21:39,040
Walter zeigt Regina gerade die Farm.
Sie kommen sicher gleich.

134
00:21:39,920 --> 00:21:43,920
Wollen Sie bei uns zu Abend essen?
Das w�r ganz nett, ja.

135
00:21:43,960 --> 00:21:49,960
Ich hab 4 Stunden Sandpiste hinter mir.
Heute fahre ich sicher nicht zur�ck.

136
00:21:52,400 --> 00:21:57,640
Ich hab Zwiebeln mitgebracht. Und Zucker.
Und Zigaretten.

137
00:21:59,520 --> 00:22:04,720
Gepriesen seist du, ewiger Gott.
Du hast uns den Schabbat gegeben.

138
00:22:05,800 --> 00:22:09,320
In Liebe und zur Erinnerung
an die Sch�pfung,

139
00:22:09,800 --> 00:22:13,800
ein Andenken an die Befreiung aus �gypten.
Gepriesen seist du Ewiger,

140
00:22:13,840 --> 00:22:17,160
der du den Schabbat heiligst.

141
00:22:20,440 --> 00:22:22,440
Danke.

142
00:22:35,160 --> 00:22:37,160
Danke.

143
00:22:38,880 --> 00:22:42,840
Gut Schabbes!
Ich wei� gar nicht, wann ich

144
00:22:42,880 --> 00:22:46,880
das letzte Mal den Kiddusch geh�rt hab.
Bisher hab ich Gott nie vermisst.

145
00:22:46,920 --> 00:22:50,880
Jeden Tag Chinin macht blind.
Aber die Malaria.

146
00:22:50,920 --> 00:22:54,920
Die kann man immer noch bek�mpfen,
wenn sie einen erwischt hat. Owuor!

147
00:22:56,600 --> 00:23:01,520
Trinken wir auf die Ankunft von Jettel
und Regina. Auf unser zweites Leben.

148
00:23:01,560 --> 00:23:04,160
Prost.

149
00:23:07,880 --> 00:23:11,880
Wie lange sind Sie weg von Zuhause?
Hier ist jetzt mein Zuhause.

150
00:23:11,920 --> 00:23:15,920
S��kind war schlau genug,
Deutschland schon '33 zu verlassen.

151
00:23:15,960 --> 00:23:20,840
Damals war die Ausreise noch einfacher.
Du konntest noch dein Geld mitnehmen, ja?

152
00:23:20,880 --> 00:23:26,880
Und vor allem deine B�cher.
Und lhre Frau? Waren Sie nie verheiratet?

153
00:23:26,920 --> 00:23:30,320
Nein. War ich nicht.
Und warum nicht?

154
00:23:30,360 --> 00:23:32,240
Jettel!

155
00:23:32,280 --> 00:23:38,760
Ich hatte immer das Pech, mich in Frauen
zu verlieben, die bereits vergeben waren.

156
00:23:41,160 --> 00:23:46,160
Meine Mutter sagt immer: Das mit den Nazis
geht nicht mehr lange so weiter.

157
00:23:46,200 --> 00:23:49,880
Deutschland ist doch ein Kulturvolk,
das Land von Goethe und Schiller.

158
00:23:49,920 --> 00:23:54,880
Papa! Ich glaub, ich hab L�wen geh�rt!
Quatschkopf! Hier gibt's keine L�wen.

159
00:23:54,920 --> 00:23:58,240
Das waren Affen. Paviane.
Vielleicht verstellt sich der L�we ja

160
00:23:58,280 --> 00:24:03,040
und spricht mit Absicht wie ein Affe.
Du bringst es in diesem Land noch weit.

161
00:24:03,080 --> 00:24:06,240
Du redest jetzt schon wie ein Neger!

162
00:24:11,280 --> 00:24:14,600
Die Luft ist k�hl und es dunkelt,

163
00:24:14,640 --> 00:24:17,640
Und ruhig flie�t der Rhein.

164
00:24:17,680 --> 00:24:21,640
Der Gipfel des Berges funkelt
im Abendsonnenschein.

165
00:24:21,680 --> 00:24:25,680
Die sch�nste Jungfrau sitzet
dort oben wunderbar.

166
00:24:25,720 --> 00:24:30,160
Ihr goldnes Geschmeide blitzet,
sie k�mmt ihr goldenes Haar.

167
00:24:30,200 --> 00:24:34,200
Sie k�mmt es mit goldenem Kamme
und singt ein Lied dabei.

168
00:24:34,240 --> 00:24:37,760
Das hat eine wundersame,
gewaltige Melodei.

169
00:24:38,600 --> 00:24:42,600
Dem Schiffer im kleinen Schiffe
ergreift es mit wildem Weh.

170
00:24:42,640 --> 00:24:47,560
Er schaut nicht die Felsenriffe,
er schaut nur hinauf in die H�h!

171
00:24:47,600 --> 00:24:51,920
Ich glaube die Wellen verschlingen
am Ende Schiffer und Kahn.

172
00:24:51,960 --> 00:24:56,600
Und das hat mit ihrem Singen...
die Loreley getan!

173
00:24:56,640 --> 00:24:59,640
Schlaf gut, mein Engel.

174
00:25:25,520 --> 00:25:29,600
Du hast den Eisschrank nicht mitgebracht,
oder?

175
00:25:29,640 --> 00:25:31,640
Jettel?

176
00:25:31,960 --> 00:25:34,960
Nein, hab ich nicht.
Wieso nicht?

177
00:25:35,000 --> 00:25:39,000
Es war kein Platz mehr in den Kisten.
Wir durften ja blo� zwei Kisten...

178
00:25:39,040 --> 00:25:42,920
Aber f�r deine Bl�mchenmuster war Platz!
Au�erdem war kein Geld mehr da.

179
00:25:42,960 --> 00:25:48,960
Was hast du mit dem ganzen Geld gemacht?
Wenn du's genau wissen willst:

180
00:25:49,000 --> 00:25:53,000
Ich hab mir das hier gekauft.
Bei Wertheim in Breslau.

181
00:25:53,040 --> 00:25:56,640
Es hat 45 Mark gekostet
und ist wundersch�n.

182
00:25:57,280 --> 00:26:00,160
Du hast dir ein Abendkleid gekauft?

183
00:26:01,280 --> 00:26:03,280
Ja.

184
00:26:17,480 --> 00:26:23,080
<i> ... seinen Namen gibt.
Und den Stempel seiner Pers�nlichkeit</i>

185
00:26:23,160 --> 00:26:26,160
<i>unausl�schlich aufdr�ckt.</i>

186
00:26:26,280 --> 00:26:30,400
Mensch, dass ich mich �ber dem seine
Stimme mal so freuen w�rde...

187
00:26:30,440 --> 00:26:35,440
Ich lass euch den Kasten da. Dann habt ihr
wenigstens etwas Kontakt zur Au�enwelt.

188
00:26:35,480 --> 00:26:41,640
Das k�nnen wir nicht annehmen.
Nat�rlich kannst du. Ich hab noch einen.

189
00:26:41,680 --> 00:26:47,200
Au�erdem hab ich dann wenigstens
einen Grund, ab und zu vorbeizukommen,

190
00:26:47,240 --> 00:26:51,240
um die Batterien aufzuladen.
M�ssen Sie jetzt fahren?

191
00:26:51,280 --> 00:26:56,280
Ich finde, in diesem Land sollten wir auf
die Form pfeifen und uns duzen.

192
00:26:56,320 --> 00:26:59,320
Findest du nicht?

193
00:27:00,760 --> 00:27:04,720
Hast du eigentlich ein Gewehr da?
Ja, wieso?

194
00:27:04,760 --> 00:27:09,240
Jetzt, wo Jettel und Regina da sind.
Morrison hat eins dagelassen.

195
00:27:09,280 --> 00:27:13,360
Viel Gl�ck mit dem Brunnen!
Danke.

196
00:27:48,920 --> 00:27:54,360
Ich darf hier nicht mehr weiter.
Mama hat gesagt, nur bis zum Rand!

197
00:28:22,680 --> 00:28:25,680
Kannst du mir helfen?

198
00:28:26,520 --> 00:28:28,520
Helfen!

199
00:30:25,840 --> 00:30:27,840
Owuor singt

200
00:30:27,880 --> 00:30:32,800
Ich hab mein Herz
in Heidelberg verloren...

201
00:31:12,280 --> 00:31:16,640
Warum schlachtet Owuor keine H�hner?
Wir d�rfen die H�hner nicht essen.

202
00:31:16,680 --> 00:31:20,600
Das ist mit Morrison so abgemacht.
Nur die Eier.

203
00:31:20,680 --> 00:31:24,680
Ich halte diesen Fra� nicht aus.
Ich brauch auch mal Fleisch!

204
00:31:24,720 --> 00:31:27,720
Danke, Owuor.

205
00:31:45,520 --> 00:31:49,520
Du musst keine Angst haben.
Das ist blo� 'n ganz normales Buschfeuer.

206
00:31:49,560 --> 00:31:53,040
Das kommt nicht bis zum Haus...

207
00:32:09,200 --> 00:32:13,240
Packen! Ich will weg!

208
00:32:13,280 --> 00:32:17,280
Ich halt's nicht mehr aus.
Das sagst du immer.

209
00:32:17,320 --> 00:32:21,320
Aber ich will zu Menschen,
deren Sprache ich verstehe!

210
00:32:21,360 --> 00:32:25,360
Du verdienst keinen Pfennig hier.
Wir essen jeden Tag Eier und Maisbrei!

211
00:32:25,400 --> 00:32:29,720
Wie soll denn Regina zur Schule gehen?
Verdammt, wir leben!

212
00:32:29,760 --> 00:32:32,760
Ja, wir leben! Und wozu?

213
00:32:33,280 --> 00:32:37,760
Um den ganzen Tag auf Regen zu hoffen,
damit die bl�den Rinder nicht krepieren,

214
00:32:37,800 --> 00:32:41,800
die uns noch nicht mal geh�ren?
Ich f�hl mich auch schon wie tot!

215
00:32:41,840 --> 00:32:45,840
Und manchmal w�nschte ich, ich w�r's.
Sag das nie wieder!

216
00:32:45,880 --> 00:32:50,000
Wir sind gerade noch davongekommen!
Was hei�t das denn wieder?

217
00:32:50,040 --> 00:32:53,280
Die Nazis haben gestern Nacht �berall
in Deutschland Synagogen angez�ndet

218
00:32:53,320 --> 00:32:58,200
und j�dische Gesch�fte gepl�ndert.
Sie haben alles kurz und klein geschlagen.

219
00:32:58,240 --> 00:33:00,880
Menschen, H�user, L�den. Alles.

220
00:33:01,880 --> 00:33:05,720
Wie willst du das auf dieser
gottverdammten Farm erfahren haben?

221
00:33:05,760 --> 00:33:11,600
Ich habe heute Fr�h um 5 den
Schweizer Sender reinbekommen.

222
00:33:15,840 --> 00:33:20,240
F�r die Nazis sind wir
keine Menschen mehr. Verflucht.

223
00:33:20,480 --> 00:33:25,200
Ich hab's kommen sehen.
Ich hab's doch immer kommen sehen!

224
00:33:25,680 --> 00:33:32,000
Kapierst du jetzt, dass es keine Rolle
spielt, wann und ob Regina lesen lernt?

225
00:33:33,080 --> 00:33:36,240
Und Mama? Und K�the? Und dein Vater?

226
00:33:37,000 --> 00:33:42,680
Ich wei� es doch auch nicht!
Ich hab immer gesagt, sie sollen da raus!

227
00:33:53,880 --> 00:33:55,880
Unverst�ndlich

228
00:33:58,600 --> 00:34:01,600
10. November 1938.

229
00:34:01,720 --> 00:34:05,720
Lieber Vater, die Nachrichten, die mich
hier �ber Deutschland erreichen,

230
00:34:05,760 --> 00:34:09,920
machen mir gro�e Sorgen. Man f�rchtet,
ein Krieg ist nicht mehr aufzuhalten.

231
00:34:09,960 --> 00:34:12,960
Was sagst du dazu?

232
00:34:13,280 --> 00:34:17,760
W�sste ich doch nur, wie es euch geht!
W�rde ich hier nur eine Mark verdienen,

233
00:34:17,800 --> 00:34:21,800
ich w�rde euch sofort nachholen!
Siehst du denn gar keinen Weg,

234
00:34:21,840 --> 00:34:25,840
Deutschland noch zu verlassen?
Ich beschw�re dich...

235
00:34:25,880 --> 00:34:29,920
Vater, wie sehr sehne ich mich
nach einem Gespr�ch mit dir,

236
00:34:29,960 --> 00:34:32,920
nach deinem Rat, deiner Anteilnahme.

237
00:34:36,280 --> 00:34:40,760
Erst hier in der Fremde wird mir klar,
wie reich mich Gott beschenkt hat

238
00:34:40,800 --> 00:34:44,800
mit einem Elternhaus wie dem meinen
und wie dankbar ich dir bin f�r alles,

239
00:34:44,840 --> 00:34:48,840
was du f�r mich getan hast. Glaub nicht,
es war rausgeschmissenes Geld,

240
00:34:48,880 --> 00:34:52,880
mich nach Mutters Tod so lange
studieren zu lassen. Ich bin sicher,

241
00:34:52,920 --> 00:34:57,920
eines Tages bin ich wieder der Anwalt,
auf den du immer so stolz warst.

242
00:35:43,760 --> 00:35:47,760
Halt es vorsichtig fest.

243
00:35:55,960 --> 00:35:57,960
Toto!

244
00:35:59,320 --> 00:36:03,280
Ih, das ist kalt!
Willst du rumlaufen wie ein Negerkind?

245
00:36:03,320 --> 00:36:07,240
Das ist mir egal.
Ich hab nichts gegen die Negerkinder.

246
00:36:07,280 --> 00:36:12,280
Es gibt ja auch keine anderen Kinder hier.
Aber sei vorsichtig! Sie haben Malaria

247
00:36:12,320 --> 00:36:16,760
und W�rmer und sind sehr, sehr schmutzig.
Ich will nicht, dass du krank wirst.

248
00:36:16,800 --> 00:36:20,720
Ein wei�es Kind ist nun mal
kein schwarzes Kind.

249
00:36:20,800 --> 00:36:25,880
Iss nichts von dem, was sie dir geben!
Und geh niemals in eine ihrer H�tten!

250
00:36:25,920 --> 00:36:27,920
So.

251
00:36:28,640 --> 00:36:30,760
Jetzt schluck das!

252
00:36:30,800 --> 00:36:34,800
Das ist so bitter. Und au�erdem werd ich
davon doch blind!

253
00:36:34,840 --> 00:36:37,920
Unfug! Mach den Mund auf!

254
00:36:40,280 --> 00:36:44,440
Du, Mama? Heute Abend wollen sie
ein Fest f�r die Ahnen feiern.

255
00:36:44,480 --> 00:36:48,800
Sie wollen ein Lamm schlachten
und ihren Gott Ngai um Regen bitten.

256
00:36:48,840 --> 00:36:52,720
Der wohnt auf dem Berg. Dem Mount Kenia.
Und im Feigenbaum auch.

257
00:36:52,760 --> 00:36:57,320
Das wird ein gro�es Fest.
Aber du bist ganz sicher nicht dabei.

258
00:36:57,360 --> 00:37:01,840
Sie bitten um Regen. Das ist wichtig.
Warum sagst du immer alles doppelt?

259
00:37:01,880 --> 00:37:05,880
Machen das die Neger auch so?
Gutes kann man nicht oft genug sagen.

260
00:37:05,920 --> 00:37:09,920
Sagt wer? Alle! Gutes kann man
nicht oft genug sagen.

261
00:37:09,960 --> 00:37:13,160
Was soll ich blo� mit dir machen?

262
00:37:13,240 --> 00:37:17,320
Wei�t du noch, die Oma?
Die hat doch immer gesagt:

263
00:37:17,520 --> 00:37:22,560
Mach dir ums Reginchen keine Sorgen.
Die hat's Massel vom Goj!

264
00:37:23,760 --> 00:37:25,720
Genau.

265
00:37:25,760 --> 00:37:29,680
'S Massel vom Goj.
Das hat die Oma immer gesagt.

266
00:37:29,760 --> 00:37:33,760
Dann brauchst du dir ja keine Sorgen
mehr zu machen, Mama.

267
00:37:33,800 --> 00:37:39,640
Ich hab so Angst, dass dir was passiert.
Musst du nicht, hier ist es doch sch�n!

268
00:37:39,680 --> 00:37:42,160
Toto? Toto!

269
00:39:53,320 --> 00:39:56,320
Schie� es doch tot.

270
00:39:59,200 --> 00:40:03,680
Ich kann das nicht!
Du siehst doch, dass es nicht geht!

271
00:40:05,160 --> 00:40:10,200
Ich wollte doch nur, dass du dein
verdammtes Fleisch bekommst.

272
00:40:38,680 --> 00:40:42,920
Gute Nacht. Du kannst ruhig noch lesen.
Das Licht st�rt mich nicht.

273
00:40:42,960 --> 00:40:46,960
Das Buch hab ich schon 3-mal gelesen.
Es ist v�llig gleichg�ltig,

274
00:40:47,000 --> 00:40:50,640
ob ich es noch ein 4. Mal lese!
Pst. Regina!

275
00:40:50,680 --> 00:40:54,680
Ich hab nicht erwartet,
dass du jagen kannst, Walter.

276
00:40:54,720 --> 00:40:58,720
Ich k�nnte's auch nicht.
Du hast doch noch nie ein Tier get�tet.

277
00:40:58,760 --> 00:41:02,760
Ich will's aber k�nnen! Ich sag dir:
Mach mich nicht zum Versager!

278
00:41:02,800 --> 00:41:06,760
Ich hab dich nicht gebeten,
auf die Jagd zu gehen.

279
00:41:06,800 --> 00:41:11,640
Du behandelst mich wie einen Auss�tzigen!
Du hast dich aber auch so ver�ndert.

280
00:41:11,680 --> 00:41:15,680
Du l�sst mich nur als Anwalt
unter deinen Rock! Das ist der Punkt!

281
00:41:15,720 --> 00:41:19,720
Verschwitzt und unrasiert l�uft nichts!
Wie du redest...

282
00:41:19,760 --> 00:41:24,280
Wie denn, Prinzessin! Ich bin dein Mann!
Ich darf dir sagen, was ich denke!

283
00:41:24,320 --> 00:41:28,880
Du hast n�mlich kein Recht
auf ein privilegiertes Leben!

284
00:41:30,080 --> 00:41:35,080
Bisher hatten wir nur Gl�ck! Also h�r auf,
das T�chterchen aus gutem Haus zu spielen

285
00:41:35,120 --> 00:41:39,320
und kapier endlich, um was es hier geht!
Wo gehst du denn hin?

286
00:41:39,360 --> 00:41:43,360
In die Kneipe. Und noch eins:
Die Art, wie du mit Owuor umgehst,

287
00:41:43,400 --> 00:41:48,400
das erinnert mich an einige Zeitgenossen
in Deutschland, mit denen du sicher nicht

288
00:41:48,440 --> 00:41:51,920
in einen Topf geworfen werden willst!

289
00:42:34,120 --> 00:42:37,720
Habari, kleiner Hund.
Wo kommst du denn her?

290
00:42:48,600 --> 00:42:53,360
Papa, der Hund hier hat kein Zuhause!
Darf ich ihn behalten? Bitte!

291
00:42:53,400 --> 00:42:57,360
Aber du hast doch Angst vor Hunden!
Nein. Hier nicht.

292
00:42:57,400 --> 00:43:00,640
Doch, der gef�llt mir, den behalten wir.

293
00:43:00,680 --> 00:43:04,680
Wir nennen ihn Rummler!
Wie den Kreisleiter aus Leobsch�tz.

294
00:43:04,720 --> 00:43:08,800
Rummler ist ein sch�nes Wort!
Genau, dann k�nnen wir jeden Tag rufen:

295
00:43:08,840 --> 00:43:14,760
"Rummler, du Mistkerl!" Und uns freuen,
dass uns niemand verhaften kommt!

296
00:43:39,920 --> 00:43:41,920
Toto!

297
00:43:42,440 --> 00:43:44,440
Toto!

298
00:44:07,160 --> 00:44:09,160
Owuor!

299
00:44:09,800 --> 00:44:11,800
Owuor!

300
00:44:26,360 --> 00:44:30,360
S��kind, was bedeutet das?
H�rt ihr kein Radio, Walter?

301
00:44:30,400 --> 00:44:34,400
Wir werden alle interniert!
Ich lasse Jettel und Regina

302
00:44:34,440 --> 00:44:38,360
nicht alleine auf der Farm lasse!
Die Engl�nder sind zuverl�ssig.

303
00:44:39,880 --> 00:44:43,880
Frauen und Kinder kommen nach Nairobi.
Sag mir, ich muss keine Angst haben.

304
00:44:43,920 --> 00:44:46,880
Alles wird gut. Ehrenwort.

305
00:44:51,640 --> 00:44:53,640
Walter!

306
00:45:00,000 --> 00:45:02,000
Regina!

307
00:45:20,280 --> 00:45:24,440
Darf ich den auch mitnehmen?
Nein. Entweder den B�r oder die Puppe.

308
00:45:24,480 --> 00:45:26,480
Asante.

309
00:45:33,280 --> 00:45:37,280
Let me help you.
Vergessen Sie lhren Ausweis nicht!

310
00:45:37,360 --> 00:45:42,640
Sprechen Sie Deutsch?
Ja. Meine Mutter ist Deutsch.

311
00:45:56,600 --> 00:45:59,680
Nein. Ich will kein neues Toto.

312
00:46:46,680 --> 00:46:50,680
Auf einmal waren wir keine Fl�chtlinge,
sondern feindliche Ausl�nder.

313
00:46:50,720 --> 00:46:54,720
Wir wussten selbst nicht genau,
warum die Engl�nder uns einsperrten.

314
00:46:54,760 --> 00:46:58,760
Wir waren zwar Deutsche und England
war mit Deutschland im Krieg,

315
00:46:58,800 --> 00:47:03,840
aber wir waren ja auch Juden
und somit kaum auf Hitlers Seite.

316
00:48:00,360 --> 00:48:03,360
Steigen Sie bitte aus, meine Damen.

317
00:48:05,080 --> 00:48:08,160
Mama, ist das ein sch�nes Gef�ngnis!

318
00:48:37,400 --> 00:48:41,240
Verstehen Sie das?
Na.

319
00:48:41,440 --> 00:48:45,440
Wahrscheinlich wussten die ned,
wohin mit uns.

320
00:48:46,080 --> 00:48:50,080
Des is bestimmt an Plumpudding!
Was is'n das?

321
00:48:50,120 --> 00:48:53,240
Woa� i net. Was Englisches.

322
00:48:57,280 --> 00:49:00,280
Entschuldigung, ist hier noch frei?

323
00:49:14,760 --> 00:49:19,600
Das erinnert mich an bessere Zeiten.
Am End m�ss ma des ois selber zahl'n!

324
00:49:19,640 --> 00:49:23,640
Der Chefkoch fuhr auf 'nem Luxusdampfer.
Der kann gar nicht anders.

325
00:49:23,680 --> 00:49:26,240
I hab an Hunger!
This is lobster.

326
00:49:26,280 --> 00:49:29,680
Fish, nice fish!
Nein, danke.

327
00:49:29,720 --> 00:49:33,680
Danke sch�n.
What's wrong with it, ladies?

328
00:49:33,720 --> 00:49:35,880
Unverst�ndliche Gespr�che

329
00:49:38,240 --> 00:49:44,560
In Breslau waren wir so oft unterwegs.
St�ndig Abendessen oder Gesellschaften.

330
00:49:45,360 --> 00:49:48,360
Das war so ein sch�nes Leben!

331
00:49:48,400 --> 00:49:53,120
Ja, ja, vor der Emigration
war jeder Dackel 'n Bernhardiner!

332
00:49:55,680 --> 00:49:58,680
Sind Sie fertig? Danke.

333
00:50:13,640 --> 00:50:16,720
No message f�r lhren Mann?

334
00:50:21,320 --> 00:50:25,320
Es ist das Gef�hl, alleine zu sein.
Zu sp�ren, dass man sich eigentlich

335
00:50:25,360 --> 00:50:29,920
nicht viel zu sagen hat. Nichts teile,
au�er miteinander verbrachte Zeit.

336
00:50:29,960 --> 00:50:33,960
Ein Kind vielleicht.
Und diese Erkenntnis ist schmerzhaft.

337
00:50:34,000 --> 00:50:37,000
Versteht du, was ich meine?
Ah ja.

338
00:50:37,040 --> 00:50:41,040
Alleinsein ist eine Sache.
Damit kenn ich mich aus.

339
00:50:41,080 --> 00:50:45,160
Aber mit einer Frau wie Jettel?
So eine sch�ne Frau!

340
00:50:46,320 --> 00:50:49,240
Sie ist so fr�hlich und lebendig.

341
00:50:49,400 --> 00:50:53,240
Vielleicht gibst du ihr
keine echte Chance.

342
00:50:53,440 --> 00:50:58,480
Daheim hat unser Leben funktioniert.
Jeder hatte seine Rolle zu spielen...

343
00:50:58,520 --> 00:51:01,040
Na, siehst du.

344
00:51:01,080 --> 00:51:05,960
Sie will die Realit�t nicht wahrhaben.
Aber ich will eine erwachsene Frau,

345
00:51:06,000 --> 00:51:09,000
mit der ich reden kann!

346
00:51:09,040 --> 00:51:13,040
Ich muss doch auch mit all dem
fertigwerden. Mein Vater und Liesel...

347
00:51:13,080 --> 00:51:16,240
Halt doch endlich mal die Schnauze!

348
00:51:16,400 --> 00:51:18,400
Oi!

349
00:51:28,400 --> 00:51:32,240
Manchmal denke ich,
wir sind wie zwei Pakete.

350
00:51:33,320 --> 00:51:37,320
Fest verschn�rt liegen wir
nebeneinander in einem Zug,

351
00:51:37,360 --> 00:51:42,360
der uns zu einem unbekannten Ziel bringt.
Wir reisen eine weite Strecke miteinander,

352
00:51:42,400 --> 00:51:46,400
aber was in dem anderen drin ist,
wissen wir nicht.

353
00:51:46,440 --> 00:51:51,880
Du machst dir zu viele Gedanken.
Vielleicht. Schlaf gut.

354
00:52:02,960 --> 00:52:04,280
Hey!

355
00:52:09,160 --> 00:52:13,160
Bald waren wir dem Hotel l�stig.
Jeden Tag gingen wir und unsere M�tter

356
00:52:13,200 --> 00:52:17,240
im Schatten spazieren und �berlegten,
was es wohl zum Abendessen gibt.

357
00:52:17,280 --> 00:52:21,280
Wenn wir wenigstens ein paar Stunden
drau�en bummeln k�nnten.

358
00:52:21,320 --> 00:52:25,320
Hier drin wird man ja wahnsinnig.
Wenn mein Mann im Lager w�re,

359
00:52:25,360 --> 00:52:29,360
w�rde ich f�r seine Freiheit k�mpfen!
Wie soll'n wir denn das machen?

360
00:52:29,400 --> 00:52:33,400
Mama, i hab schon wieder Hunger!
Ihr seid doch alle j�disch.

361
00:52:33,440 --> 00:52:37,280
Schreibt einen Brief an eure Gemeinde
in Nairobi!

362
00:52:37,520 --> 00:52:42,520
Die Juden haben doch was zu sagen hier.
Kann denn jemand von uns Englisch?

363
00:52:42,560 --> 00:52:44,560
I scho!

364
00:52:44,720 --> 00:52:49,720
Die j�dische Gemeinde kl�rte schnell,
dass nicht alle Deutschen in diesem Krieg

365
00:52:49,760 --> 00:52:54,760
auf Hitlers Seite standen. 2 Wochen sp�ter
durften wir die M�nner im Lager besuchen.

366
00:52:54,800 --> 00:52:59,760
In ihren khakifarbenen Uniformen waren
unsere gefangenen V�ter kaum von den

367
00:52:59,800 --> 00:53:02,840
englischen Soldaten zu unterscheiden.

368
00:53:14,760 --> 00:53:19,640
Papa! Ich habe eine richtige Freundin!
Sie hei�t Inge. Sie kann schon lesen!

369
00:53:19,680 --> 00:53:23,800
Und Mama hat einen Brief geschrieben!
Was f�r einen Brief?

370
00:53:23,840 --> 00:53:28,240
Einen richtigen Brief,
damit wir dich besuchen d�rfen!

371
00:53:38,880 --> 00:53:42,880
Du hast an die Engl�nder geschrieben?
Nicht ich! Alle Frauen zusammen.

372
00:53:42,920 --> 00:53:46,920
Vielleicht k�nnen wir bald nach Rongai!
Was haben sie denn mit dir gemacht?

373
00:53:46,960 --> 00:53:50,960
Du warst doch todungl�cklich in Rongai.
Wollt ihr was trinken?

374
00:53:51,000 --> 00:53:54,960
Alle wollen wieder zur�ck
auf ihre Farmen!

375
00:53:55,000 --> 00:53:57,160
Unverst�ndliches Gespr�ch

376
00:54:02,640 --> 00:54:05,600
Wir k�nnen nicht nach Rongai zur�ck.
Was?

377
00:54:05,640 --> 00:54:09,640
Ich bin gek�ndigt. Wegen dem Krieg.
Morrison will keinen "Enemy Alien"

378
00:54:09,680 --> 00:54:13,680
auf seiner Farm besch�ftigen.
Und was bedeutet das jetzt?

379
00:54:13,720 --> 00:54:18,440
Dass ich im Moment keine Arbeit habe
und wir kein Zuhause.

380
00:54:24,040 --> 00:54:28,040
Auf jeden Fall m�ssen wir irgendwann
unsere Sachen von der Farm holen.

381
00:54:28,080 --> 00:54:33,080
Vielleicht wartet er ja noch auf uns.
Weil er doch denkt, dass wir wiederkommen.

382
00:54:33,120 --> 00:54:37,120
Vielleicht trifft Papa Owuor ja dann.
Bringt er ihn dann hierher mit?

383
00:54:37,160 --> 00:54:40,160
Bestimmt. Bestimmt.

384
00:55:18,560 --> 00:55:22,240
Kann ich ihnen vielleicht helfen?
Ja. Bitte. Danke!

385
00:55:22,280 --> 00:55:26,760
Ich brauche eine speschal permischon.
Ich muss Herrn Edward Rubens besuchen,

386
00:55:26,800 --> 00:55:31,160
den Vorsitzenden der j�dischen Gemeinde.
Ich muss ihn dringend sprechen. Bitte.

387
00:55:37,480 --> 00:55:41,240
Here we are.
Please help yourself, Mrs. Redlich.

388
00:55:41,480 --> 00:55:46,280
Mein Mann und ich, wir have been '36,
for the Olympics, in Berlin!

389
00:55:46,320 --> 00:55:50,320
Mein erste Mal in Deutschland.
My God, we were naiv.

390
00:55:50,360 --> 00:55:55,360
Wir glaubten auch bis zum bitteren Ende,
dass unsere zivilisierten Freunde

391
00:55:55,400 --> 00:55:59,400
Hitler stoppen w�rden.
Nonsense! Es gab nie Freundschaft

392
00:55:59,440 --> 00:56:03,440
zwischen Deutschen und Juden!
Das sehe ich anders, wir hatten...

393
00:56:03,480 --> 00:56:07,480
Wo sind sie jetzt? Wo waren sie in '38?
H�tte es Hitler nicht gegeben...

394
00:56:07,520 --> 00:56:11,520
Hitler erfand den Antisemitismus nicht!
I don't think, Mrs Redlich came here

395
00:56:11,560 --> 00:56:16,400
to discuss politics with you! Oder?
Wie k�nnen wir helfen, my dear?

396
00:56:16,440 --> 00:56:20,440
Mr. Morrison, der Besitzer unserer Farm
m�chte keine "Enemy Aliens mehr.

397
00:56:20,480 --> 00:56:24,480
Aber ohne Arbeit kann Walter das Lager
nicht verlassen und wir wissen nicht,

398
00:56:24,520 --> 00:56:28,800
wie wir neue Arbeit finden sollen.
Deshalb bitte ich Sie um Hilfe.

399
00:56:28,840 --> 00:56:33,680
Wir w�rden alles tun.
Deshalb kommen Sie zu mir?

400
00:56:33,720 --> 00:56:38,720
Glauben Sie, Sie sind die Einzige,
die unter diesem bl�dsinnigen Krieg leidet?

401
00:56:38,760 --> 00:56:42,760
Oder sind wir f�r Sie verantwortlich,
nur weil wir Sie immigrieren lie�en?

402
00:56:42,800 --> 00:56:46,800
Sie sind am Leben! Sie sind hier!
By God, machen Sie was draus.

403
00:56:46,840 --> 00:56:52,280
Eddie! I'm sorry, I can't help you!
Excuse me.

404
00:56:54,800 --> 00:56:57,800
Wir haben Verwandte in Polen.

405
00:56:57,840 --> 00:57:01,800
Mein Mann hat seit Monaten
nichts mehr von ihnen geh�rt.

406
00:57:01,840 --> 00:57:03,880
Das tut mir Leid.

407
00:57:03,920 --> 00:57:08,640
Well, good luck, my dear. Please take
these Biscuits for your little girl.

408
00:57:08,680 --> 00:57:12,120
Wiedersehen.

409
00:57:53,480 --> 00:57:59,000
Was machen Sie denn hier?
Ihr Ausflug, war der erfolgreich?

410
00:58:05,080 --> 00:58:08,080
Wieso, was meinen Sie?

411
00:58:10,520 --> 00:58:14,680
Sie suchen einen Job. F�r lhren Mann.
Auf einer Farm.

412
00:58:15,080 --> 00:58:19,160
Ich habe einen Freund, hier bei der Army.

413
00:58:19,360 --> 00:58:22,360
Er braucht jemanden.

414
00:58:23,960 --> 00:58:27,200
Ich k�nnte mit ihm sprechen.

415
00:58:30,520 --> 00:58:33,720
Das w�re aber sehr nett von Ihnen.

416
00:58:34,400 --> 00:58:37,400
...6... 7... 8...

417
00:58:44,240 --> 00:58:47,240
You're very beautiful, Mrs. Redlich.

418
00:59:12,920 --> 00:59:16,160
Wo bist du denn? Post f�r dich.

419
00:59:16,840 --> 00:59:20,000
Sieht ziemlich wichtig aus.

420
00:59:20,880 --> 00:59:24,880
Yesterday... today... zum Kuckuck,
was hei�t jetzt wieder "morgen"?

421
00:59:24,920 --> 00:59:28,960
"Tomorrow" hei�t das!
Vielleicht merkste dir das jetzt mal!

422
00:59:29,000 --> 00:59:33,240
Tomorrow, there will be rain in Kenia.
Na siehste, geht doch.

423
00:59:33,280 --> 00:59:37,440
Pussycat, pussycat, where have you been?
I've been to London, to see the Queen!

424
00:59:37,480 --> 00:59:43,120
Pussycat, pussycat, what did you there?
I caught a little mouse under the chair!

425
00:59:43,160 --> 00:59:46,040
Och Mensch, Regina!

426
00:59:46,080 --> 00:59:49,560
Walter!
Wie hast 'n das nur hingekriegt?

427
00:59:49,600 --> 00:59:56,040
Was hab ich denn hingekriegt?
My daddy! My daddy is back from prison!

428
01:00:22,520 --> 01:00:26,840
Papa! Der heilige Berg da sieht aus
wie'n Chinesenhut!

429
01:00:26,880 --> 01:00:30,880
Hey! Kommst du da runter?
Du brichst dir noch den Hals!

430
01:00:30,920 --> 01:00:33,880
Das ist der sch�nste Platz der Welt!

431
01:00:38,800 --> 01:00:40,800
Danke.

432
01:00:43,240 --> 01:00:46,320
S��kind, ich hab Angst.
Angst? Wovor?

433
01:00:46,360 --> 01:00:48,360
Dieser Platz hier ist noch weiter entfernt
von deiner Farm als Rongai.

434
01:00:48,400 --> 01:00:53,800
Du kannst uns nur selten besuchen.
Daf�r fahr ich auch 20 Meilen mehr.

435
01:00:53,840 --> 01:00:55,840
Versprochen?

436
01:00:55,960 --> 01:00:59,240
Ich werde �fter kommen, als dir lieb ist!

437
01:01:12,960 --> 01:01:16,920
Die verdammten Rinder sind wir los.
Wie geht's?

438
01:01:16,960 --> 01:01:23,440
Sehr gut.
Nzuri sana, memsaab? Geht's dir gut?

439
01:01:24,320 --> 01:01:28,280
Ich bin dein Bwana und befehle dir...
Na was, Bwana?

440
01:01:28,320 --> 01:01:32,160
Zieh deine Bluse aus!
Bist du meschugge?

441
01:01:32,200 --> 01:01:35,720
Du bekommst auch daf�r
ein gebratenes Huhn.

442
01:01:41,800 --> 01:01:46,280
Und jetzt lauf den Weg da runter.
Wie eine Afrikanerin.

443
01:02:02,000 --> 01:02:07,360
Hast du mich vermisst in deinem Hotel?
Au! Was soll denn das?

444
01:05:15,520 --> 01:05:17,280
Einen Koch!

***