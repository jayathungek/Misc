1
00:01:07,08 --> 00:01:11,080
An Deutschland konnte ich mich
gar nicht mehr richtig erinnern.

2
00:01:11,12 --> 00:01:15,120
Ich wusste noch,
dass es da Schnee gibt und Jahreszeiten.

3
00:01:15,16 --> 00:01:19,240
Und das unsere Familie da war.
Alle, nicht nur Mama und Papa. Eben alle.

4
00:01:19,28 --> 00:01:22,760
Und dass mir das gefallen hat.

5
00:01:24,32 --> 00:01:28,320
Aber ich wei� auch noch, dass ich
in Deutschland immer Angst hatte.

6
00:01:28,36 --> 00:01:34,680
Vor anderen Kindern und vor den Menschen
auf der Stra�e. Und sogar vor Hunden.

7
00:01:39,16 --> 00:01:41,400
Na komm. Komm, ich helf dir.

8
00:01:54,52 --> 00:01:59,520
Deutschland ist ein dunkler Ort f�r mich.
Nicht so hell und hei� wie Kenia.

9
00:01:59,56 --> 00:02:03,520
Ein Ort mit gro�en Geb�uden
und mit d�steren Zimmern.

10
00:02:03,56 --> 00:02:08,560
Deutschland ist unsere Heimat, sagt Papa.
Er vermisst wohl vor allem seinen Vater.

11
00:02:08,60 --> 00:02:13,560
Meinen Opa. Er hatte eine Nussallergie
und durfte keine Nusspl�tzchen essen.

12
00:02:13,60 --> 00:02:19,440
Er mochte Heinrich Heines Gedichte gern.
Vor allem die �ber Deutschland.

13
00:02:22,00 --> 00:02:25,320
Ich hatte auch zwei Tanten dort,
die ich furchtbar gern mochte.

14
00:02:25,36 --> 00:02:29,360
Tante K�the war Mamas Schwester.
Papas Schwester hie� Liesel.

15
00:02:29,40 --> 00:02:33,360
Manchmal halfen sie in Gro�vaters
Hotel "Adler" aus.

16
00:02:33,40 --> 00:02:37,960
Aber das war in den guten Zeiten,
bevor die Nazis kamen.

17
00:02:44,60 --> 00:02:47,600
Soll ich Ihnen helfen?

18
00:02:47,64 --> 00:02:50,640
Nein, danke.

19
00:03:02,92 --> 00:03:05,920
Wir m�ssen jetzt fahren.

20
00:03:08,68 --> 00:03:12,680
Die Nazis verboten Papa,
als Rechtsanwalt zu arbeiten

21
00:03:12,72 --> 00:03:17,240
und nahmen Gro�vater das Hotel weg.
Dann war die ganze Familie arbeitslos.

22
00:03:17,28 --> 00:03:22,320
Fragte ich meine Eltern, warum das so ist,
bekam ich immer die gleiche Antwort:

23
00:03:22,36 --> 00:03:25,400
Weil wir Juden sind, Regina. Deshalb.

24
00:04:13,32 --> 00:04:16,320
Bwana! Bwana! Bwana!

25
00:04:17,44 --> 00:04:20,440
Bwana! Bwana! Bwana!

26
00:04:22,56 --> 00:04:26,560
- Was ist denn los?
- Eine eilige Nachricht.

27
00:04:51,92 --> 00:04:53,920
Walter.

28
00:04:54,16 --> 00:04:57,760
Walter! Wach auf!
Das ist Chinin! Das ist gut.

29
00:05:00,44 --> 00:05:03,440
Komm, wach auf!

30
00:05:11,28 --> 00:05:15,000
Menschenskinder, Walter, wach auf!

31
00:05:28,56 --> 00:05:31,640
Walter, mach den Mund auf!

32
00:05:37,64 --> 00:05:42,040
Entschuldigung, ich muss mal eben
die T�r aufmachen. Augenblick.

33
00:05:42,08 --> 00:05:47,240
Tante Jettel, gibt es noch Apfelsaft?
Kleinen Augenblick. Ja?

34
00:05:48,48 --> 00:05:53,400
Anna! Sch�n dass ihr kommen konntet
bei dem Schnee. Rudolf, kommt rein.

35
00:05:53,44 --> 00:05:57,480
'N Abend, Frau Redlich.
Du sollst doch nicht mit den reden!

36
00:05:59,88 --> 00:06:06,280
Anna, hallo.
Hallo, Ina.

37
00:06:07,48 --> 00:06:11,800
Hubert, J�rgen! H�rt auf zu z�ndeln!
Ihr steckt ja noch das ganze Haus an.

38
00:06:11,84 --> 00:06:14,840
Ja, ja, Klara.

39
00:06:17,28 --> 00:06:21,360
Hier, bei� mal.
Daran wirst du dich gew�hnen m�ssen.

40
00:06:21,40 --> 00:06:26,240
Die Neger essen alles so verbrannt.
Oder ganz roh. Und blutig!

41
00:06:26,28 --> 00:06:30,280
Aber Hunde essen die schon!
Das wei� ich aus meinem Erdkundebuch.

42
00:06:30,32 --> 00:06:34,920
Die essen ja auch Heuschrecken. Halt mal!
Was willst du denn in Afrika?

43
00:06:34,96 --> 00:06:40,400
Du traust dich doch nicht mal,
einen Dackel anzufassen.

44
00:06:40,44 --> 00:06:44,440
Ist das mit N�ssen?
Es gab nur das eine, Herr Redlich!

45
00:06:44,48 --> 00:06:48,440
Aber, Klara.
So viele N�sse sind ja nicht drin!

46
00:06:48,48 --> 00:06:52,440
Weil du davon niesen musst, gell?
Ja, bei N�ssen muss ich niesen.

47
00:06:52,48 --> 00:06:55,480
Jedes verflixte Mal.

48
00:06:57,16 --> 00:07:01,560
Das Gr�ne hab ich geliebt. Walter hatte
es mir mal zu Silvester gekauft.

49
00:07:01,60 --> 00:07:05,680
Aber irgendwie steht's mir nicht mehr.
Ich find's todschick.

50
00:07:05,72 --> 00:07:08,720
Dann nimm's.

51
00:07:10,24 --> 00:07:13,240
Bist 'n Schatz, Jettel. Danke.

52
00:07:13,76 --> 00:07:18,240
K�thchen. Keine Tr�nen heute, ja?
Mal ehrlich, kann ich die tragen?

53
00:07:18,28 --> 00:07:22,360
Steh halt zu deinem Hintern, Liesel.
M�nner m�gen's rund. Frau Redlich?

54
00:07:22,40 --> 00:07:26,360
Das ist was f�r Sie, Klara.

55
00:07:26,40 --> 00:07:29,400
Ein bisschen Glitzer tut Ihnen gut.

56
00:07:29,44 --> 00:07:32,880
Was ist?

57
00:07:33,64 --> 00:07:36,640
Der ist aus Afrika.

58
00:07:41,72 --> 00:07:44,880
Rongai, Kenia, 2. Dezember 1937.

59
00:07:45,28 --> 00:07:50,280
<i>Geliebte Jettel. Ich kann mir denken,
wie aufgeregt du �ber diesen Brief bist,</i>

60
00:07:50,32 --> 00:07:54,320
<i>aber ich bitte dich, jetzt stark zu sein.
Die j�dische Gemeinde in Nairobi</i>

61
00:07:54,36 --> 00:07:59,320
<i>�bernimmt auch die Geb�hren f�r
eure Einwanderung. Nun ist es soweit.</i>

62
00:07:59,36 --> 00:08:04,920
<i>Nach 6 Monaten kann ich Regina und dich
endlich hierher nachkommen lassen.</i>

63
00:08:04,96 --> 00:08:09,080
<i>Bitte z�gere keinen Tag l�nger!
Geh sofort zu Karl Silbermann.</i>

64
00:08:09,12 --> 00:08:14,080
<i>Er kann dir mit den Schiffskarten helfen.
Sag ihm, welches Schiff es ist und</i>

65
00:08:14,12 --> 00:08:19,120
<i>wie lange es unterwegs ist, ist egal.
Hauptsache, es nimmt euch beide mit.</i>

66
00:08:19,16 --> 00:08:24,160
<i>Wir brauchen dringend einen Eisschrank.
Wenn er nicht mehr in unsere Kisten passt,</i>

67
00:08:24,20 --> 00:08:28,240
<i>schmei� daf�r das Rosenthal-Geschirr raus.
Das ist hier v�llig unbedeutend.</i>

68
00:08:28,28 --> 00:08:31,960
<i>Besorge noch ein paar Petromaxlampen,
au�erdem Moskitonetze und feste Schuhe</i>

69
00:08:32,00 --> 00:08:36,360
<i>f�r dich und Regina. Versuch ja nicht,
Bargeld oder Schmuck mitzunehmen.</i>

70
00:08:36,40 --> 00:08:40,800
<i>Du kennst die Nazis. Sprich so wenig
wie m�glich �ber eure Pl�ne.</i>

71
00:08:40,84 --> 00:08:44,840
<i>Man kann niemandem mehr trauen,
auch nicht den Menschen,</i>

72
00:08:44,88 --> 00:08:49,880
<i>die eben noch unsere besten Freunde waren.
Mein Herz zerspringt bei dem Gedanken,</i>

73
00:08:49,92 --> 00:08:54,920
<i>euch beide bald in die Arme zu schlie�en.
Es wird schwer, wenn ich mir vorstelle,</i>

74
00:08:54,96 --> 00:08:58,560
<i>wie weh dieser Brief
deiner Mutter tun wird.</i>

75
00:09:00,68 --> 00:09:03,880
Walter? H�r zu! Du hast Malaria.

76
00:09:04,28 --> 00:09:08,680
Du musst dieses Chinin hier nehmen.
H�rst du, das ist ganz wichtig!

77
00:09:08,72 --> 00:09:12,720
Ich muss zur�ck zur Farm.
Owuor wird sich um dich k�mmern.

78
00:09:40,40 --> 00:09:44,640
Max, was ist? Wir m�ssen los!
Ich hasse Abschiede.

79
00:09:44,68 --> 00:09:47,880
Und Bahnh�fe kann ich auch nicht leiden.

80
00:09:50,68 --> 00:09:54,680
In ein, zwei Jahren
ist das hier doch alles vorbei.

81
00:10:02,76 --> 00:10:06,160
Ihr haltet zusammen, ihr zwei, ja?

82
00:10:07,68 --> 00:10:11,440
Versprich mir,
dass ihr das zusammen durchsteht.

83
00:10:16,32 --> 00:10:20,240
Einer liebt immer mehr.
Das macht's so schwierig.

84
00:10:21,96 --> 00:10:25,080
Und der, der mehr liebt ist verwundbar.

85
00:10:32,08 --> 00:10:35,240
Mein Sohn liebt dich. Sehr.

86
00:10:38,96 --> 00:10:41,280
Du wirst mir so fehlen, Max.

87
00:10:48,56 --> 00:10:50,560
Mama!

88
00:11:09,32 --> 00:11:12,880
Sag ihr Auf Wiedersehen. Bitte.

89
00:12:16,68 --> 00:12:21,640
Kennst du Schlesisches Himmelreich?
Schlesische Himme...

90
00:12:21,68 --> 00:12:26,960
Fleisch, Kartoffeln, Speck, D�rrobst,
alles zusammengemischt. Mmh!

91
00:12:32,76 --> 00:12:36,760
In meinem ersten Leben war ich Anwalt,
wei�t du?

92
00:12:39,44 --> 00:12:43,520
Kikombe. Kikombe!

93
00:12:43,56 --> 00:12:45,560
Kikombe.

94
00:12:45,68 --> 00:12:49,640
Kiyo la dirisha.
Kiyo la dirisha.

95
00:12:49,68 --> 00:12:52,680
Kiyo la di...

96
00:12:53,48 --> 00:12:56,480
Dirisha la Kiyo.

97
00:12:57,12 --> 00:13:00,360
Asante kapissa.

98
00:14:09,08 --> 00:14:12,080
Owuor? Das ist f�r dich.

99
00:14:15,96 --> 00:14:19,480
Ich war krank und du hast mich gepflegt.

100
00:14:23,32 --> 00:14:27,320
Ich hab diese Robe
in meiner Heimat getragen.

101
00:14:30,40 --> 00:14:33,680
Sondern mit meinem Kopf gearbeitet habe.

102
00:14:39,44 --> 00:14:42,600
Nein. F�r eine Robe muss man klug sein.

103
00:14:46,56 --> 00:14:49,080
Hm.

104
00:15:04,08 --> 00:15:08,640
6 Wochen fuhren wir mit einem
Passsagierdampfer einmal um ganz Afrika.

105
00:15:08,68 --> 00:15:12,800
Am 20. April 1938, dem Geburtstag
des F�hrers, kamen wir in Nairobi an.

106
00:15:12,84 --> 00:15:15,840
Regina, nimm das mal bitte.

107
00:15:19,20 --> 00:15:22,320
Mr. Morrison, der Besitzer der Rinderfarm,
auf der Papa arbeitete, holte uns ab.

108
00:15:46,60 --> 00:15:50,600
Ich musste an Schokolade denken.
Aber Mama erkl�rte mir auf dem Schiff,

109
00:15:50,64 --> 00:15:54,880
dass wir jetzt arm waren. Und f�r arme
Kinder gibt es keine Schokolade.

110
00:15:54,92 --> 00:15:58,360
Ich hatte ihr versprochen,
tapfer zu sein.

111
00:17:26,64 --> 00:17:30,240
Liebste!

112
00:18:07,84 --> 00:18:12,240
Regina, mein M�dchen! Komm zu mir!
Papa! Wir sind mit dem Schiff gefahren!

113
00:18:12,28 --> 00:18:17,120
Und waren in England und Marokko und
�berall am Hafen. Ich hab Delfine gesehen!

114
00:18:17,16 --> 00:18:21,160
Und Mama hat auch mal getanzt!

115
00:19:28,48 --> 00:19:30,480
Kinderlachen

116
00:19:50,76 --> 00:19:54,760
Es gibt zu wenig Wasser f�r die Rinder.
F�r welche Rinder?

117
00:19:54,80 --> 00:19:58,960
F�r alle Rinder.
Dein Vater hat die Papiere besorgt.

118
00:19:59,00 --> 00:20:04,000
Und die Fluchtsteuer. Wir hatten Gl�ck.
In Hamburg �ffneten sie nur eine Kiste.

119
00:20:04,04 --> 00:20:08,040
Da war nur W�sche drin. Max gab mir
den "Zauberberg" f�r dich mit.

120
00:20:08,08 --> 00:20:12,640
Die B�cher waren in der anderen Kiste.
Gott sei Dank.

121
00:20:12,68 --> 00:20:16,720
Ich wei�, es ist nicht das,
was du dir erwartet hast.

122
00:20:16,76 --> 00:20:19,760
Es ist wundersch�n.

123
00:20:20,92 --> 00:20:24,040
Aber hier k�nnen wir doch nicht leben.

124
00:20:36,28 --> 00:20:40,600
Die Sachen mit dem Muster nicht.
Das brauchen wir nicht alles auspacken.

125
00:20:40,64 --> 00:20:43,280
Wir bleiben ja nicht lange hier.

126
00:20:57,72 --> 00:21:01,040
Memsaab. Teller. Ein Teller.

127
00:21:01,68 --> 00:21:07,200
Wenn du mit mir reden willst,
musst du schon Deutsch lernen. Teller!

128
00:21:12,52 --> 00:21:16,360
Vorsichtig! Ja.

129
00:21:17,52 --> 00:21:19,520
Teller!

130
00:21:49,08 --> 00:21:52,080
Jettel? Mein Name ist S��kind.

131
00:21:52,72 --> 00:21:56,240
Auch Walter.
Aber alle nennen mich S��kind.

132
00:21:56,40 --> 00:21:58,960
Ach so.

133
00:21:59,00 --> 00:22:04,040
Walter zeigt Regina gerade die Farm.
Sie kommen sicher gleich.

134
00:22:04,92 --> 00:22:08,920
Wollen Sie bei uns zu Abend essen?
Das w�r ganz nett, ja.

135
00:22:08,96 --> 00:22:14,960
Ich hab 4 Stunden Sandpiste hinter mir.
Heute fahre ich sicher nicht zur�ck.

136
00:22:17,40 --> 00:22:22,640
Ich hab Zwiebeln mitgebracht. Und Zucker.
Und Zigaretten.

137
00:22:24,52 --> 00:22:29,720
Gepriesen seist du, ewiger Gott.
Du hast uns den Schabbat gegeben.

138
00:22:30,80 --> 00:22:34,320
In Liebe und zur Erinnerung
an die Sch�pfung,

139
00:22:34,80 --> 00:22:38,800
ein Andenken an die Befreiung aus �gypten.
Gepriesen seist du Ewiger,

140
00:22:38,84 --> 00:22:42,160
der du den Schabbat heiligst.

141
00:22:45,44 --> 00:22:47,440
Danke.

142
00:23:00,16 --> 00:23:02,160
Danke.

143
00:23:03,88 --> 00:23:07,840
Gut Schabbes!
Ich wei� gar nicht, wann ich

144
00:23:07,88 --> 00:23:11,880
das letzte Mal den Kiddusch geh�rt hab.
Bisher hab ich Gott nie vermisst.

145
00:23:11,92 --> 00:23:15,880
Jeden Tag Chinin macht blind.
Aber die Malaria.

146
00:23:15,92 --> 00:23:19,920
Die kann man immer noch bek�mpfen,
wenn sie einen erwischt hat. Owuor!

147
00:23:21,60 --> 00:23:26,520
Trinken wir auf die Ankunft von Jettel
und Regina. Auf unser zweites Leben.

148
00:23:26,56 --> 00:23:29,160
Prost.

149
00:23:32,88 --> 00:23:36,880
Wie lange sind Sie weg von Zuhause?
Hier ist jetzt mein Zuhause.

150
00:23:36,92 --> 00:23:40,920
S��kind war schlau genug,
Deutschland schon '33 zu verlassen.

151
00:23:40,96 --> 00:23:45,840
Damals war die Ausreise noch einfacher.
Du konntest noch dein Geld mitnehmen, ja?

152
00:23:45,88 --> 00:23:51,880
Und vor allem deine B�cher.
Und lhre Frau? Waren Sie nie verheiratet?

153
00:23:51,92 --> 00:23:55,320
Nein. War ich nicht.
Und warum nicht?

154
00:23:55,36 --> 00:23:57,240
Jettel!

155
00:23:57,28 --> 00:24:03,760
Ich hatte immer das Pech, mich in Frauen
zu verlieben, die bereits vergeben waren.

156
00:24:06,16 --> 00:24:11,160
Meine Mutter sagt immer: Das mit den Nazis
geht nicht mehr lange so weiter.

157
00:24:11,20 --> 00:24:14,880
Deutschland ist doch ein Kulturvolk,
das Land von Goethe und Schiller.

158
00:24:14,92 --> 00:24:19,880
Papa! Ich glaub, ich hab L�wen geh�rt!
Quatschkopf! Hier gibt's keine L�wen.

159
00:24:19,92 --> 00:24:23,240
Das waren Affen. Paviane.
Vielleicht verstellt sich der L�we ja

160
00:24:23,28 --> 00:24:28,040
und spricht mit Absicht wie ein Affe.
Du bringst es in diesem Land noch weit.

161
00:24:28,08 --> 00:24:31,240
Du redest jetzt schon wie ein Neger!

162
00:24:36,28 --> 00:24:39,600
Die Luft ist k�hl und es dunkelt,

163
00:24:39,64 --> 00:24:42,640
Und ruhig flie�t der Rhein.

164
00:24:42,68 --> 00:24:46,640
Der Gipfel des Berges funkelt
im Abendsonnenschein.

165
00:24:46,68 --> 00:24:50,680
Die sch�nste Jungfrau sitzet
dort oben wunderbar.

166
00:24:50,72 --> 00:24:55,160
Ihr goldnes Geschmeide blitzet,
sie k�mmt ihr goldenes Haar.

167
00:24:55,20 --> 00:24:59,200
Sie k�mmt es mit goldenem Kamme
und singt ein Lied dabei.

168
00:24:59,24 --> 00:25:02,760
Das hat eine wundersame,
gewaltige Melodei.

169
00:25:03,60 --> 00:25:07,600
Dem Schiffer im kleinen Schiffe
ergreift es mit wildem Weh.

170
00:25:07,64 --> 00:25:12,560
Er schaut nicht die Felsenriffe,
er schaut nur hinauf in die H�h!

171
00:25:12,60 --> 00:25:16,920
Ich glaube die Wellen verschlingen
am Ende Schiffer und Kahn.

172
00:25:16,96 --> 00:25:21,600
Und das hat mit ihrem Singen...
die Loreley getan!

173
00:25:21,64 --> 00:25:24,640
Schlaf gut, mein Engel.

174
00:25:50,52 --> 00:25:54,600
Du hast den Eisschrank nicht mitgebracht,
oder?

175
00:25:54,64 --> 00:25:56,640
Jettel?

176
00:25:56,96 --> 00:25:59,960
Nein, hab ich nicht.
Wieso nicht?

177
00:26:00,00 --> 00:26:04,000
Es war kein Platz mehr in den Kisten.
Wir durften ja blo� zwei Kisten...

178
00:26:04,04 --> 00:26:07,920
Aber f�r deine Bl�mchenmuster war Platz!
Au�erdem war kein Geld mehr da.

179
00:26:07,96 --> 00:26:13,960
Was hast du mit dem ganzen Geld gemacht?
Wenn du's genau wissen willst:

180
00:26:14,00 --> 00:26:18,000
Ich hab mir das hier gekauft.
Bei Wertheim in Breslau.

181
00:26:18,04 --> 00:26:21,640
Es hat 45 Mark gekostet
und ist wundersch�n.

182
00:26:22,28 --> 00:26:25,160
Du hast dir ein Abendkleid gekauft?

183
00:26:26,28 --> 00:26:28,280
Ja.

184
00:26:42,48 --> 00:26:48,080
<i> ... seinen Namen gibt.
Und den Stempel seiner Pers�nlichkeit</i>

185
00:26:48,16 --> 00:26:51,160
<i>unausl�schlich aufdr�ckt.</i>

186
00:26:51,28 --> 00:26:55,400
Mensch, dass ich mich �ber dem seine
Stimme mal so freuen w�rde...

187
00:26:55,44 --> 00:27:00,440
Ich lass euch den Kasten da. Dann habt ihr
wenigstens etwas Kontakt zur Au�enwelt.

188
00:27:00,48 --> 00:27:06,640
Das k�nnen wir nicht annehmen.
Nat�rlich kannst du. Ich hab noch einen.

189
00:27:06,68 --> 00:27:12,200
Au�erdem hab ich dann wenigstens
einen Grund, ab und zu vorbeizukommen,

190
00:27:12,24 --> 00:27:16,240
um die Batterien aufzuladen.
M�ssen Sie jetzt fahren?

191
00:27:16,28 --> 00:27:21,280
Ich finde, in diesem Land sollten wir auf
die Form pfeifen und uns duzen.

192
00:27:21,32 --> 00:27:24,320
Findest du nicht?

193
00:27:25,76 --> 00:27:29,720
Hast du eigentlich ein Gewehr da?
Ja, wieso?

194
00:27:29,76 --> 00:27:34,240
Jetzt, wo Jettel und Regina da sind.
Morrison hat eins dagelassen.

195
00:27:34,28 --> 00:27:38,360
Viel Gl�ck mit dem Brunnen!
Danke.

196
00:28:13,92 --> 00:28:19,360
Ich darf hier nicht mehr weiter.
Mama hat gesagt, nur bis zum Rand!

197
00:28:47,68 --> 00:28:50,680
Kannst du mir helfen?

198
00:28:51,52 --> 00:28:53,520
Helfen!

199
00:30:50,84 --> 00:30:52,840
Owuor singt

200
00:30:52,88 --> 00:30:57,800
Ich hab mein Herz
in Heidelberg verloren...

201
00:31:37,28 --> 00:31:41,640
Warum schlachtet Owuor keine H�hner?
Wir d�rfen die H�hner nicht essen.

202
00:31:41,68 --> 00:31:45,600
Das ist mit Morrison so abgemacht.
Nur die Eier.

203
00:31:45,68 --> 00:31:49,680
Ich halte diesen Fra� nicht aus.
Ich brauch auch mal Fleisch!

204
00:31:49,72 --> 00:31:52,720
Danke, Owuor.

205
00:32:10,52 --> 00:32:14,520
Du musst keine Angst haben.
Das ist blo� 'n ganz normales Buschfeuer.

206
00:32:14,56 --> 00:32:18,040
Das kommt nicht bis zum Haus...

207
00:32:34,20 --> 00:32:38,240
Packen! Ich will weg!

208
00:32:38,28 --> 00:32:42,280
Ich halt's nicht mehr aus.
Das sagst du immer.

209
00:32:42,32 --> 00:32:46,320
Aber ich will zu Menschen,
deren Sprache ich verstehe!

210
00:32:46,36 --> 00:32:50,360
Du verdienst keinen Pfennig hier.
Wir essen jeden Tag Eier und Maisbrei!

211
00:32:50,40 --> 00:32:54,720
Wie soll denn Regina zur Schule gehen?
Verdammt, wir leben!

212
00:32:54,76 --> 00:32:57,760
Ja, wir leben! Und wozu?

213
00:32:58,28 --> 00:33:02,760
Um den ganzen Tag auf Regen zu hoffen,
damit die bl�den Rinder nicht krepieren,

214
00:33:02,80 --> 00:33:06,800
die uns noch nicht mal geh�ren?
Ich f�hl mich auch schon wie tot!

215
00:33:06,84 --> 00:33:10,840
Und manchmal w�nschte ich, ich w�r's.
Sag das nie wieder!

216
00:33:10,88 --> 00:33:15,000
Wir sind gerade noch davongekommen!
Was hei�t das denn wieder?

217
00:33:15,04 --> 00:33:18,280
Die Nazis haben gestern Nacht �berall
in Deutschland Synagogen angez�ndet

218
00:33:18,32 --> 00:33:23,200
und j�dische Gesch�fte gepl�ndert.
Sie haben alles kurz und klein geschlagen.

219
00:33:23,24 --> 00:33:25,880
Menschen, H�user, L�den. Alles.

220
00:33:26,88 --> 00:33:30,720
Wie willst du das auf dieser
gottverdammten Farm erfahren haben?

221
00:33:30,76 --> 00:33:36,600
Ich habe heute Fr�h um 5 den
Schweizer Sender reinbekommen.

222
00:33:40,84 --> 00:33:45,240
F�r die Nazis sind wir
keine Menschen mehr. Verflucht.

223
00:33:45,48 --> 00:33:50,200
Ich hab's kommen sehen.
Ich hab's doch immer kommen sehen!

224
00:33:50,68 --> 00:33:57,000
Kapierst du jetzt, dass es keine Rolle
spielt, wann und ob Regina lesen lernt?

225
00:33:58,08 --> 00:34:01,240
Und Mama? Und K�the? Und dein Vater?

226
00:34:02,00 --> 00:34:07,680
Ich wei� es doch auch nicht!
Ich hab immer gesagt, sie sollen da raus!

227
00:34:18,88 --> 00:34:20,880
Unverst�ndlich

228
00:34:23,60 --> 00:34:26,600
10. November 1938.

229
00:34:26,72 --> 00:34:30,720
Lieber Vater, die Nachrichten, die mich
hier �ber Deutschland erreichen,

230
00:34:30,76 --> 00:34:34,920
machen mir gro�e Sorgen. Man f�rchtet,
ein Krieg ist nicht mehr aufzuhalten.

231
00:34:34,96 --> 00:34:37,960
Was sagst du dazu?

232
00:34:38,28 --> 00:34:42,760
W�sste ich doch nur, wie es euch geht!
W�rde ich hier nur eine Mark verdienen,

233
00:34:42,80 --> 00:34:46,800
ich w�rde euch sofort nachholen!
Siehst du denn gar keinen Weg,

234
00:34:46,84 --> 00:34:50,840
Deutschland noch zu verlassen?
Ich beschw�re dich...

235
00:34:50,88 --> 00:34:54,920
Vater, wie sehr sehne ich mich
nach einem Gespr�ch mit dir,

236
00:34:54,96 --> 00:34:57,920
nach deinem Rat, deiner Anteilnahme.

237
00:35:01,28 --> 00:35:05,760
Erst hier in der Fremde wird mir klar,
wie reich mich Gott beschenkt hat

238
00:35:05,80 --> 00:35:09,800
mit einem Elternhaus wie dem meinen
und wie dankbar ich dir bin f�r alles,

239
00:35:09,84 --> 00:35:13,840
was du f�r mich getan hast. Glaub nicht,
es war rausgeschmissenes Geld,

240
00:35:13,88 --> 00:35:17,880
mich nach Mutters Tod so lange
studieren zu lassen. Ich bin sicher,

241
00:35:17,92 --> 00:35:22,920
eines Tages bin ich wieder der Anwalt,
auf den du immer so stolz warst.

242
00:36:08,76 --> 00:36:12,760
Halt es vorsichtig fest.

243
00:36:20,96 --> 00:36:22,960
Toto!

244
00:36:24,32 --> 00:36:28,280
Ih, das ist kalt!
Willst du rumlaufen wie ein Negerkind?

245
00:36:28,32 --> 00:36:32,240
Das ist mir egal.
Ich hab nichts gegen die Negerkinder.

246
00:36:32,28 --> 00:36:37,280
Es gibt ja auch keine anderen Kinder hier.
Aber sei vorsichtig! Sie haben Malaria

247
00:36:37,32 --> 00:36:41,760
und W�rmer und sind sehr, sehr schmutzig.
Ich will nicht, dass du krank wirst.

248
00:36:41,80 --> 00:36:45,720
Ein wei�es Kind ist nun mal
kein schwarzes Kind.

249
00:36:45,80 --> 00:36:50,880
Iss nichts von dem, was sie dir geben!
Und geh niemals in eine ihrer H�tten!

250
00:36:50,92 --> 00:36:52,920
So.

251
00:36:53,64 --> 00:36:55,760
Jetzt schluck das!

252
00:36:55,80 --> 00:36:59,800
Das ist so bitter. Und au�erdem werd ich
davon doch blind!

253
00:36:59,84 --> 00:37:02,920
Unfug! Mach den Mund auf!

254
00:37:05,28 --> 00:37:09,440
Du, Mama? Heute Abend wollen sie
ein Fest f�r die Ahnen feiern.

255
00:37:09,48 --> 00:37:13,800
Sie wollen ein Lamm schlachten
und ihren Gott Ngai um Regen bitten.

256
00:37:13,84 --> 00:37:17,720
Der wohnt auf dem Berg. Dem Mount Kenia.
Und im Feigenbaum auch.

257
00:37:17,76 --> 00:37:22,320
Das wird ein gro�es Fest.
Aber du bist ganz sicher nicht dabei.

258
00:37:22,36 --> 00:37:26,840
Sie bitten um Regen. Das ist wichtig.
Warum sagst du immer alles doppelt?

259
00:37:26,88 --> 00:37:30,880
Machen das die Neger auch so?
Gutes kann man nicht oft genug sagen.

260
00:37:30,92 --> 00:37:34,920
Sagt wer? Alle! Gutes kann man
nicht oft genug sagen.

261
00:37:34,96 --> 00:37:38,160
Was soll ich blo� mit dir machen?

262
00:37:38,24 --> 00:37:42,320
Wei�t du noch, die Oma?
Die hat doch immer gesagt:

263
00:37:42,52 --> 00:37:47,560
Mach dir ums Reginchen keine Sorgen.
Die hat's Massel vom Goj!

264
00:37:48,76 --> 00:37:50,720
Genau.

265
00:37:50,76 --> 00:37:54,680
'S Massel vom Goj.
Das hat die Oma immer gesagt.

266
00:37:54,76 --> 00:37:58,760
Dann brauchst du dir ja keine Sorgen
mehr zu machen, Mama.

267
00:37:58,80 --> 00:38:04,640
Ich hab so Angst, dass dir was passiert.
Musst du nicht, hier ist es doch sch�n!

268
00:38:04,68 --> 00:38:07,160
Toto? Toto!

269
00:40:18,32 --> 00:40:21,320
Schie� es doch tot.

270
00:40:24,20 --> 00:40:28,680
Ich kann das nicht!
Du siehst doch, dass es nicht geht!

271
00:40:30,16 --> 00:40:35,200
Ich wollte doch nur, dass du dein
verdammtes Fleisch bekommst.

272
00:41:03,68 --> 00:41:07,920
Gute Nacht. Du kannst ruhig noch lesen.
Das Licht st�rt mich nicht.

273
00:41:07,96 --> 00:41:11,960
Das Buch hab ich schon 3-mal gelesen.
Es ist v�llig gleichg�ltig,

274
00:41:12,00 --> 00:41:15,640
ob ich es noch ein 4. Mal lese!
Pst. Regina!

275
00:41:15,68 --> 00:41:19,680
Ich hab nicht erwartet,
dass du jagen kannst, Walter.

276
00:41:19,72 --> 00:41:23,720
Ich k�nnte's auch nicht.
Du hast doch noch nie ein Tier get�tet.

277
00:41:23,76 --> 00:41:27,760
Ich will's aber k�nnen! Ich sag dir:
Mach mich nicht zum Versager!

278
00:41:27,80 --> 00:41:31,760
Ich hab dich nicht gebeten,
auf die Jagd zu gehen.

279
00:41:31,80 --> 00:41:36,640
Du behandelst mich wie einen Auss�tzigen!
Du hast dich aber auch so ver�ndert.

280
00:41:36,68 --> 00:41:40,680
Du l�sst mich nur als Anwalt
unter deinen Rock! Das ist der Punkt!

281
00:41:40,72 --> 00:41:44,720
Verschwitzt und unrasiert l�uft nichts!
Wie du redest...

282
00:41:44,76 --> 00:41:49,280
Wie denn, Prinzessin! Ich bin dein Mann!
Ich darf dir sagen, was ich denke!

283
00:41:49,32 --> 00:41:53,880
Du hast n�mlich kein Recht
auf ein privilegiertes Leben!

284
00:41:55,08 --> 00:42:00,080
Bisher hatten wir nur Gl�ck! Also h�r auf,
das T�chterchen aus gutem Haus zu spielen

285
00:42:00,12 --> 00:42:04,320
und kapier endlich, um was es hier geht!
Wo gehst du denn hin?

286
00:42:04,36 --> 00:42:08,360
In die Kneipe. Und noch eins:
Die Art, wie du mit Owuor umgehst,

287
00:42:08,40 --> 00:42:13,400
das erinnert mich an einige Zeitgenossen
in Deutschland, mit denen du sicher nicht

288
00:42:13,44 --> 00:42:16,920
in einen Topf geworfen werden willst!

289
00:42:59,12 --> 00:43:02,720
Habari, kleiner Hund.
Wo kommst du denn her?

290
00:43:13,60 --> 00:43:18,360
Papa, der Hund hier hat kein Zuhause!
Darf ich ihn behalten? Bitte!

291
00:43:18,40 --> 00:43:22,360
Aber du hast doch Angst vor Hunden!
Nein. Hier nicht.

292
00:43:22,40 --> 00:43:25,640
Doch, der gef�llt mir, den behalten wir.

293
00:43:25,68 --> 00:43:29,680
Wir nennen ihn Rummler!
Wie den Kreisleiter aus Leobsch�tz.

294
00:43:29,72 --> 00:43:33,800
Rummler ist ein sch�nes Wort!
Genau, dann k�nnen wir jeden Tag rufen:

295
00:43:33,84 --> 00:43:39,760
"Rummler, du Mistkerl!" Und uns freuen,
dass uns niemand verhaften kommt!

296
00:44:04,92 --> 00:44:06,920
Toto!

297
00:44:07,44 --> 00:44:09,440
Toto!

298
00:44:32,16 --> 00:44:34,160
Owuor!

299
00:44:34,80 --> 00:44:36,800
Owuor!

300
00:44:51,36 --> 00:44:55,360
S��kind, was bedeutet das?
H�rt ihr kein Radio, Walter?

301
00:44:55,40 --> 00:44:59,400
Wir werden alle interniert!
Ich lasse Jettel und Regina

302
00:44:59,44 --> 00:45:03,360
nicht alleine auf der Farm lasse!
Die Engl�nder sind zuverl�ssig.

303
00:45:04,88 --> 00:45:08,880
Frauen und Kinder kommen nach Nairobi.
Sag mir, ich muss keine Angst haben.

304
00:45:08,92 --> 00:45:11,880
Alles wird gut. Ehrenwort.

305
00:45:16,64 --> 00:45:18,640
Walter!

306
00:45:25,00 --> 00:45:27,000
Regina!

307
00:45:45,28 --> 00:45:49,440
Darf ich den auch mitnehmen?
Nein. Entweder den B�r oder die Puppe.

308
00:45:49,48 --> 00:45:51,480
Asante.

309
00:45:58,28 --> 00:46:02,280
Let me help you.
Vergessen Sie lhren Ausweis nicht!

310
00:46:02,36 --> 00:46:07,640
Sprechen Sie Deutsch?
Ja. Meine Mutter ist Deutsch.

311
00:46:21,60 --> 00:46:24,680
Nein. Ich will kein neues Toto.

312
00:47:11,68 --> 00:47:15,680
Auf einmal waren wir keine Fl�chtlinge,
sondern feindliche Ausl�nder.

313
00:47:15,72 --> 00:47:19,720
Wir wussten selbst nicht genau,
warum die Engl�nder uns einsperrten.

314
00:47:19,76 --> 00:47:23,760
Wir waren zwar Deutsche und England
war mit Deutschland im Krieg,

315
00:47:23,80 --> 00:47:28,840
aber wir waren ja auch Juden
und somit kaum auf Hitlers Seite.

316
00:48:25,36 --> 00:48:28,360
Steigen Sie bitte aus, meine Damen.

317
00:48:30,08 --> 00:48:33,160
Mama, ist das ein sch�nes Gef�ngnis!

318
00:49:02,40 --> 00:49:06,240
Verstehen Sie das?
Na.

319
00:49:06,44 --> 00:49:10,440
Wahrscheinlich wussten die ned,
wohin mit uns.

320
00:49:11,08 --> 00:49:15,080
Des is bestimmt an Plumpudding!
Was is'n das?

321
00:49:15,12 --> 00:49:18,240
Woa� i net. Was Englisches.

322
00:49:22,28 --> 00:49:25,280
Entschuldigung, ist hier noch frei?

323
00:49:39,76 --> 00:49:44,600
Das erinnert mich an bessere Zeiten.
Am End m�ss ma des ois selber zahl'n!

324
00:49:44,64 --> 00:49:48,640
Der Chefkoch fuhr auf 'nem Luxusdampfer.
Der kann gar nicht anders.

325
00:49:48,68 --> 00:49:51,240
I hab an Hunger!
This is lobster.

326
00:49:51,28 --> 00:49:54,680
Fish, nice fish!
Nein, danke.

327
00:49:54,72 --> 00:49:58,680
Danke sch�n.
What's wrong with it, ladies?

328
00:49:58,72 --> 00:50:00,880
Unverst�ndliche Gespr�che

329
00:50:03,24 --> 00:50:09,560
In Breslau waren wir so oft unterwegs.
St�ndig Abendessen oder Gesellschaften.

330
00:50:10,36 --> 00:50:13,360
Das war so ein sch�nes Leben!

331
00:50:13,40 --> 00:50:18,120
Ja, ja, vor der Emigration
war jeder Dackel 'n Bernhardiner!

332
00:50:20,68 --> 00:50:23,680
Sind Sie fertig? Danke.

333
00:50:38,64 --> 00:50:41,720
No message f�r lhren Mann?

334
00:50:46,32 --> 00:50:50,320
Es ist das Gef�hl, alleine zu sein.
Zu sp�ren, dass man sich eigentlich

335
00:50:50,36 --> 00:50:54,920
nicht viel zu sagen hat. Nichts teile,
au�er miteinander verbrachte Zeit.

336
00:50:54,96 --> 00:50:58,960
Ein Kind vielleicht.
Und diese Erkenntnis ist schmerzhaft.

337
00:50:59,00 --> 00:51:02,000
Versteht du, was ich meine?
Ah ja.

338
00:51:02,04 --> 00:51:06,040
Alleinsein ist eine Sache.
Damit kenn ich mich aus.

339
00:51:06,08 --> 00:51:10,160
Aber mit einer Frau wie Jettel?
So eine sch�ne Frau!

340
00:51:11,32 --> 00:51:14,240
Sie ist so fr�hlich und lebendig.

341
00:51:14,40 --> 00:51:18,240
Vielleicht gibst du ihr
keine echte Chance.

342
00:51:18,44 --> 00:51:23,480
Daheim hat unser Leben funktioniert.
Jeder hatte seine Rolle zu spielen...

343
00:51:23,52 --> 00:51:26,040
Na, siehst du.

344
00:51:26,08 --> 00:51:30,960
Sie will die Realit�t nicht wahrhaben.
Aber ich will eine erwachsene Frau,

345
00:51:31,00 --> 00:51:34,000
mit der ich reden kann!

346
00:51:34,04 --> 00:51:38,040
Ich muss doch auch mit all dem
fertigwerden. Mein Vater und Liesel...

347
00:51:38,08 --> 00:51:41,240
Halt doch endlich mal die Schnauze!

348
00:51:41,40 --> 00:51:43,400
Oi!

349
00:51:53,40 --> 00:51:57,240
Manchmal denke ich,
wir sind wie zwei Pakete.

350
00:51:58,32 --> 00:52:02,320
Fest verschn�rt liegen wir
nebeneinander in einem Zug,

351
00:52:02,36 --> 00:52:07,360
der uns zu einem unbekannten Ziel bringt.
Wir reisen eine weite Strecke miteinander,

352
00:52:07,40 --> 00:52:11,400
aber was in dem anderen drin ist,
wissen wir nicht.

353
00:52:11,44 --> 00:52:16,880
Du machst dir zu viele Gedanken.
Vielleicht. Schlaf gut.

354
00:52:27,96 --> 00:52:29,280
Hey!

355
00:52:34,16 --> 00:52:38,160
Bald waren wir dem Hotel l�stig.
Jeden Tag gingen wir und unsere M�tter

356
00:52:38,20 --> 00:52:42,240
im Schatten spazieren und �berlegten,
was es wohl zum Abendessen gibt.

357
00:52:42,28 --> 00:52:46,280
Wenn wir wenigstens ein paar Stunden
drau�en bummeln k�nnten.

358
00:52:46,32 --> 00:52:50,320
Hier drin wird man ja wahnsinnig.
Wenn mein Mann im Lager w�re,

359
00:52:50,36 --> 00:52:54,360
w�rde ich f�r seine Freiheit k�mpfen!
Wie soll'n wir denn das machen?

360
00:52:54,40 --> 00:52:58,400
Mama, i hab schon wieder Hunger!
Ihr seid doch alle j�disch.

361
00:52:58,44 --> 00:53:02,280
Schreibt einen Brief an eure Gemeinde
in Nairobi!

362
00:53:02,52 --> 00:53:07,520
Die Juden haben doch was zu sagen hier.
Kann denn jemand von uns Englisch?

363
00:53:07,56 --> 00:53:09,560
I scho!

364
00:53:09,72 --> 00:53:14,720
Die j�dische Gemeinde kl�rte schnell,
dass nicht alle Deutschen in diesem Krieg

365
00:53:14,76 --> 00:53:19,760
auf Hitlers Seite standen. 2 Wochen sp�ter
durften wir die M�nner im Lager besuchen.

366
00:53:19,80 --> 00:53:24,760
In ihren khakifarbenen Uniformen waren
unsere gefangenen V�ter kaum von den

367
00:53:24,80 --> 00:53:27,840
englischen Soldaten zu unterscheiden.

368
00:53:39,76 --> 00:53:44,640
Papa! Ich habe eine richtige Freundin!
Sie hei�t Inge. Sie kann schon lesen!

369
00:53:44,68 --> 00:53:48,800
Und Mama hat einen Brief geschrieben!
Was f�r einen Brief?

370
00:53:48,84 --> 00:53:53,240
Einen richtigen Brief,
damit wir dich besuchen d�rfen!

371
00:54:03,88 --> 00:54:07,880
Du hast an die Engl�nder geschrieben?
Nicht ich! Alle Frauen zusammen.

372
00:54:07,92 --> 00:54:11,920
Vielleicht k�nnen wir bald nach Rongai!
Was haben sie denn mit dir gemacht?

373
00:54:11,96 --> 00:54:15,960
Du warst doch todungl�cklich in Rongai.
Wollt ihr was trinken?

374
00:54:16,00 --> 00:54:19,960
Alle wollen wieder zur�ck
auf ihre Farmen!

375
00:54:20,00 --> 00:54:22,160
Unverst�ndliches Gespr�ch

376
00:54:27,64 --> 00:54:30,600
Wir k�nnen nicht nach Rongai zur�ck.
Was?

377
00:54:30,64 --> 00:54:34,640
Ich bin gek�ndigt. Wegen dem Krieg.
Morrison will keinen "Enemy Alien"

378
00:54:34,68 --> 00:54:38,680
auf seiner Farm besch�ftigen.
Und was bedeutet das jetzt?

379
00:54:38,72 --> 00:54:43,440
Dass ich im Moment keine Arbeit habe
und wir kein Zuhause.

380
00:54:49,04 --> 00:54:53,040
Auf jeden Fall m�ssen wir irgendwann
unsere Sachen von der Farm holen.

381
00:54:53,08 --> 00:54:58,080
Vielleicht wartet er ja noch auf uns.
Weil er doch denkt, dass wir wiederkommen.

382
00:54:58,12 --> 00:55:02,120
Vielleicht trifft Papa Owuor ja dann.
Bringt er ihn dann hierher mit?

383
00:55:02,16 --> 00:55:05,160
Bestimmt. Bestimmt.

384
00:55:43,56 --> 00:55:47,240
Kann ich ihnen vielleicht helfen?
Ja. Bitte. Danke!

385
00:55:47,28 --> 00:55:51,760
Ich brauche eine speschal permischon.
Ich muss Herrn Edward Rubens besuchen,

386
00:55:51,80 --> 00:55:56,160
den Vorsitzenden der j�dischen Gemeinde.
Ich muss ihn dringend sprechen. Bitte.

387
00:56:02,48 --> 00:56:06,240
Here we are.
Please help yourself, Mrs. Redlich.

388
00:56:06,48 --> 00:56:11,280
Mein Mann und ich, wir have been '36,
for the Olympics, in Berlin!

389
00:56:11,32 --> 00:56:15,320
Mein erste Mal in Deutschland.
My God, we were naiv.

390
00:56:15,36 --> 00:56:20,360
Wir glaubten auch bis zum bitteren Ende,
dass unsere zivilisierten Freunde

391
00:56:20,40 --> 00:56:24,400
Hitler stoppen w�rden.
Nonsense! Es gab nie Freundschaft

392
00:56:24,44 --> 00:56:28,440
zwischen Deutschen und Juden!
Das sehe ich anders, wir hatten...

393
00:56:28,48 --> 00:56:32,480
Wo sind sie jetzt? Wo waren sie in '38?
H�tte es Hitler nicht gegeben...

394
00:56:32,52 --> 00:56:36,520
Hitler erfand den Antisemitismus nicht!
I don't think, Mrs Redlich came here

395
00:56:36,56 --> 00:56:41,400
to discuss politics with you! Oder?
Wie k�nnen wir helfen, my dear?

396
00:56:41,44 --> 00:56:45,440
Mr. Morrison, der Besitzer unserer Farm
m�chte keine "Enemy Aliens mehr.

397
00:56:45,48 --> 00:56:49,480
Aber ohne Arbeit kann Walter das Lager
nicht verlassen und wir wissen nicht,

398
00:56:49,52 --> 00:56:53,800
wie wir neue Arbeit finden sollen.
Deshalb bitte ich Sie um Hilfe.

399
00:56:53,84 --> 00:56:58,680
Wir w�rden alles tun.
Deshalb kommen Sie zu mir?

400
00:56:58,72 --> 00:57:03,720
Glauben Sie, Sie sind die Einzige,
die unter diesem bl�dsinnigen Krieg leidet?

401
00:57:03,76 --> 00:57:07,760
Oder sind wir f�r Sie verantwortlich,
nur weil wir Sie immigrieren lie�en?

402
00:57:07,80 --> 00:57:11,800
Sie sind am Leben! Sie sind hier!
By God, machen Sie was draus.

403
00:57:11,84 --> 00:57:17,280
Eddie! I'm sorry, I can't help you!
Excuse me.

404
00:57:19,80 --> 00:57:22,800
Wir haben Verwandte in Polen.

405
00:57:22,84 --> 00:57:26,800
Mein Mann hat seit Monaten
nichts mehr von ihnen geh�rt.

406
00:57:26,84 --> 00:57:28,880
Das tut mir Leid.

407
00:57:28,92 --> 00:57:33,640
Well, good luck, my dear. Please take
these Biscuits for your little girl.

408
00:57:33,68 --> 00:57:37,120
Wiedersehen.

409
00:58:18,48 --> 00:58:24,000
Was machen Sie denn hier?
Ihr Ausflug, war der erfolgreich?

410
00:58:30,08 --> 00:58:33,080
Wieso, was meinen Sie?

411
00:58:35,52 --> 00:58:39,680
Sie suchen einen Job. F�r lhren Mann.
Auf einer Farm.

412
00:58:40,08 --> 00:58:44,160
Ich habe einen Freund, hier bei der Army.

413
00:58:44,36 --> 00:58:47,360
Er braucht jemanden.

414
00:58:48,96 --> 00:58:52,200
Ich k�nnte mit ihm sprechen.

415
00:58:55,52 --> 00:58:58,720
Das w�re aber sehr nett von Ihnen.

416
00:58:59,40 --> 00:59:02,400
...6... 7... 8...

417
00:59:09,24 --> 00:59:12,240
You're very beautiful, Mrs. Redlich.

418
00:59:37,92 --> 00:59:41,160
Wo bist du denn? Post f�r dich.

419
00:59:41,84 --> 00:59:45,000
Sieht ziemlich wichtig aus.

420
00:59:45,88 --> 00:59:49,880
Yesterday... today... zum Kuckuck,
was hei�t jetzt wieder "morgen"?

421
00:59:49,92 --> 00:59:53,960
"Tomorrow" hei�t das!
Vielleicht merkste dir das jetzt mal!

422
00:59:54,00 --> 00:59:58,240
Tomorrow, there will be rain in Kenia.
Na siehste, geht doch.

423
00:59:58,28 --> 01:00:02,440
Pussycat, pussycat, where have you been?
I've been to London, to see the Queen!

424
01:00:02,48 --> 01:00:08,120
Pussycat, pussycat, what did you there?
I caught a little mouse under the chair!

425
01:00:08,16 --> 01:00:11,040
Och Mensch, Regina!

426
01:00:11,08 --> 01:00:14,560
Walter!
Wie hast 'n das nur hingekriegt?

427
01:00:14,60 --> 01:00:21,040
Was hab ich denn hingekriegt?
My daddy! My daddy is back from prison!

428
01:00:47,52 --> 01:00:51,840
Papa! Der heilige Berg da sieht aus
wie'n Chinesenhut!

429
01:00:51,88 --> 01:00:55,880
Hey! Kommst du da runter?
Du brichst dir noch den Hals!

430
01:00:55,92 --> 01:00:58,880
Das ist der sch�nste Platz der Welt!

431
01:01:03,80 --> 01:01:05,800
Danke.

432
01:01:08,24 --> 01:01:11,320
S��kind, ich hab Angst.
Angst? Wovor?

433
01:01:11,36 --> 01:01:13,360
Dieser Platz hier ist noch weiter entfernt
von deiner Farm als Rongai.

434
01:01:13,40 --> 01:01:18,800
Du kannst uns nur selten besuchen.
Daf�r fahr ich auch 20 Meilen mehr.

435
01:01:18,84 --> 01:01:20,840
Versprochen?

436
01:01:20,96 --> 01:01:24,240
Ich werde �fter kommen, als dir lieb ist!

437
01:01:37,96 --> 01:01:41,920
Die verdammten Rinder sind wir los.
Wie geht's?

438
01:01:41,96 --> 01:01:48,440
Sehr gut.
Nzuri sana, memsaab? Geht's dir gut?

439
01:01:49,32 --> 01:01:53,280
Ich bin dein Bwana und befehle dir...
Na was, Bwana?

440
01:01:53,32 --> 01:01:57,160
Zieh deine Bluse aus!
Bist du meschugge?

441
01:01:57,20 --> 01:02:00,720
Du bekommst auch daf�r
ein gebratenes Huhn.

442
01:02:06,80 --> 01:02:11,280
Und jetzt lauf den Weg da runter.
Wie eine Afrikanerin.

443
01:02:27,00 --> 01:02:32,360
Hast du mich vermisst in deinem Hotel?
Au! Was soll denn das?

444
01:05:40,52 --> 01:05:42,280
Einen Koch!

**
